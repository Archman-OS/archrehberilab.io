.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
Arch Linux Rehberi (Gayri Resmi Belgedir (Unofficial Document)
##################

**GAYRI RESMİ BELGEDİR - UNOFFICIAL DOCUMENT**

**Katkı Sağlamak İstiyorum!**

Dökümana olan katkılarınızı sabit bir format üzerinden kendi oluşturmuş olduğumuz bir .ott uzantılı tema ile almaktayız. 
Sağlamak istediğiniz katkı değerlendirme süreçlerinden başarılı bir şekilde geçtikten sonra dökümantasyona eklenir.
Aksi takdirde yapmış olduğunuz katkı dökümantasyona eklenmez.
Katkı sayfasına `buradan <https://gitlab.com/Archman-OS/archlinux_kullanici_kilavuzu>`_ ulaşabilirsiniz.


**İçerik Listesi:**

.. toctree::
    :hidden:
    :maxdepth: 2
   
   docs/katkivegelisim
   docs/calismahakkinda
   docs/archlinux
   docs/archtarzi
   docs/pacmanpaketyoneticisi
   docs/kurulumrehberi
   docs/varolanlinuxdagitimiuzerindenarchlinuxkurulumu
   docs/masaustuortamiyapilandirmalari
   docs/pencereyoneticisiyapilandirmalari
   docs/uygulamalarinyuklenmesi
   docs/hatacozumleri