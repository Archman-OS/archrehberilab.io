Kurulum Rehberi
###############
Kurulum Özellikleri
*******************
    • Bu rehberde anlatılan adımların tamamı sırasıyla gerçek makinede denenerek test edilmiştir.
    • GPT Diskler göz önüne alınarak anlatım yapılmıştır.
    • Kurulum EFI olarak yapılmıştır.

Resmi Kurulum Dökümantasyonu
****************************
https://wiki.archlinux.org/index.php/Installation_Guide

Kurulum Medyasının Hazırlanması
*******************************
www.archlinux.org/download adresine giriniz. Bu kısımda BitTorrent Download (recommended) kısmında yer alan Magnet veya Torrent seçiminizi yaparak indirmenizi tamamlayabilirsiniz.

Ayrıca “HTTP Direct Downloads” kısmında listelenmiş şekilde birçok ülkeyi görebilirsiniz. Türkiye’yi seçtiğimizde şu linke gitmiş olacağız. http://ftp.linux.org.tr/archlinux/iso/2018.11.01/ 

Bu linkten de indirmemizi tamamlayabiliriz.

Rüfus kullanarak Kurulum Medyasının Hazırlanması
************************************************
.iso uzantılı kurulum medyamızı indirdikten sonra eğer Windows kullanıyorsanız Rufus programı ile bu iso dosyasını bir USB’ye yazdıracağız.

Rufus programını aşağıdaki linkten indirebilirsiniz.

https://rufus-usb.tr.uptodown.com/windows   

Kullanacağınız Rufus programı 3.0 versiyonu ve üzeriyse bölümlendirme şemasından GPT seçeneğini seçip start butonuna bastıktan sonra DD IMAGE MODE’u seçerek yazdırmalısınız.

Kullanacağınız Rufus programı 3.0 versiyonu altı ise alttaki açılır menüden DD IMAGE MODE ile yazdırmalısınız.

DD kullanımı ile Kurulum Medyasının Hazırlanması
************************************************
>>> $ dd if=archlinux.img of=/dev/sdb bs=16M && sync
    
USB Flash Kurulum Medyası hakkında daha fazla bilgiyi aşağıdaki linkten bulabilirsiniz.

https://wiki.archlinux.org/index.php/USB_flash_installation_media

.. figure:: /img/kurulumrehberi/1.png

Klavyenin Türkçeleştirilmesi
****************************
Uçbirim üzerinde yazmış olduğumuz klavyeyi değiştirmek için KEYMAP ataması yani tuş eşleme işlemi yaparak Q Klavye yapmalıyız.

.. figure:: /img/kurulumrehberi/2.png

>>> # loadkeys trq

İnternete Bağlanma
******************
İnternete bağlanmak için aşağıdaki komutu kullanacağız.

>>> # wifi-menu

İnternet Bağlantı Kontrolü
**************************
.. figure:: /img/kurulumrehberi/3.png

>>> # ping -c 4 www.archlinux.org >>>

>>> PING apollo.archlinux.org (138.201.81.199) 56(84) bytes of data.

>>> 64 bytes from apollo.archlinux.org (138.201.81.199): icmp_seq=1 ttl=51 time=67.0 ms

>>> 64 bytes from apollo.archlinux.org (138.201.81.199): icmp_seq=2 ttl=51 time=61.8 ms

>>> 64 bytes from apollo.archlinux.org (138.201.81.199): icmp_seq=3 ttl=51 time=62.8 ms

>>> 64 bytes from apollo.archlinux.org (138.201.81.199): icmp_seq=4 ttl=51 time=62.3 ms

>>> --- apollo.archlinux.org ping statistics ---

>>> 4 packets transmitted, 4 received, 0% packet loss, time 7ms

>>> rtt min/avg/max/mdev = 61.762/63.470/67.048/2.098 ms

UEFI,GPT,MBR Örnek Düzenleri
****************************
Kurulumumuzu UEFI/GPT, BIOS/MBR, UEFI/BIOS planlarında gerçekleştirebilirsiniz. Ben bu belgede UEFI/GPT planına göre anlatım yapacağım. Şimdi aşağıda bu 3 farklı plan için oluşturulan ArchWiki’den alıntılamış olduğum örnekleri inceleyiniz.

UEFI/GPT Örnek Düzeni
=====================
.. figure:: /img/kurulumrehberi/4.png

BIOS/MBR Örnek Düzeni
=====================
.. figure:: /img/kurulumrehberi/5.png

BIOS/GPT Örnek Düzeni
=====================
.. figure:: /img/kurulumrehberi/6.png

Bölümleme Araçlarının Belirlenmesi
**********************************
Disk bölümlerini yönetmek için 2 farklı seçenek olduğunu belirttik. Diskimizin GPT mi yoksa MBR mı olduğunu öğrendikten sonra bu seçimimize göre bir bölümleme aracı seçmemiz gerekiyor.

MBR ve GPT hakkında daha fazla bilgi için aşağıdaki linki inceleyiniz.

https://wiki.archlinux.org/index.php/Partitioning

.. figure:: /img/kurulumrehberi/7.png

cfdisk kullanmaya karar verdim. Sizde kendinize göre bir bölümleme aracı seçerek devam ediniz.

.. figure:: /img/kurulumrehberi/8.png

Bir önceki örnek düzen sayfasından yararlanarak UEFI/GPT bölümünde EFI alanı 550M olarak önerilmiş ancak ben 300M ‘ ın yeterli olacağını düşünüyorum. 300M EFIve 200G Linux Dosya Sistemi oluşturacağım. EFI bölümüm  FAT, Linux dosya sistemim ise ext4 formatında olacaktır.

>>> # cfdisk /dev/sdb

Komutuyla karşımıza gelen panel ile diskimizi oluşturuyoruz.
>>> dev/sdb1		2M				Bıos Boot Partition

>>> dev/sdb2		300M			EFI SYSTEM

>>> dev/sdb3		1G				Swap

>>> dev/sdb4		223.3G			Linux File System

Yukarıdaki ayarlara göre diskimi böldüm ve kaydettim.

.. figure:: /img/kurulumrehberi/9.png

.. figure:: /img/kurulumrehberi/10.png

Disklerin Biçimlendirilmesi
***************************
.. figure:: /img/kurulumrehberi/11.png

>>> # mkfs.fat -F32 /dev/sdxx

>>> # mkfs.ext4 /dev/sdxx

>>> # mkswap /dev/

Disklerin Bağlanması
********************
.. figure:: /img/kurulumrehberi/12.png

/mnt dizini ve alt dizinleri CDROM’lar, Disketler, USB vb.. depolama aygıtlarının bağlanması için geçici bağlama noktaları olmak üzere tasarlanmıştır. Bizde biçimlendirmiş olduğumuz bu diskleri geçici olarak bağlayarak işlemler yapacağız.
İlk önce dosya sistemimizi /mnt ‘ ye bağlayalım.

>>> # mount /dev/sdxx /mnt

Ardından içiçe 2 adet dizin oluşturmalıyız.

>>> # mkdir -p /mnt/boot/EFI

Ardından EFI kısmımıza bu bölümü bağlayalım.

>>> # mount /dev/sdxx /mnt/boot/EFI

Şimdi Swap alanımızı bağlayalım

>>> # swapon /dev/sdxx

Temel paket kurulumları
***********************
Sistemimize şu anda pacstrap yardımı ile base ve base-devel gruplarını kuracağız.Base grubunda 52 paket ve base-devel grubunda ise 27 adet paket mevcuttur. Paketlerin büyük bir kısmı core deposunda olduğu için bu kısımda depo ayarlarında bir değişiklik yapmamıza ihtiyaç yoktur. Base ve Base-devel gruplarının içindeki paketleri görüntülemek ve daha fazla detaylı bilgi almak istiyorsanız aşağıdaki linkleri görüntüleyebilirsiniz.

https://www.archlinux.org/groups/x86_64/base/

https://www.archlinux.org/groups/x86_64/base-devel/ 

.. figure:: /img/kurulumrehberi/13.png

.. figure:: /img/kurulumrehberi/14.png

>>> # pacstrap /mnt base base-devel

Genfstab ve Fstab
*****************
Genfstab, bir fstab dosyasına ek olarak root tarafından verilen bir bağlama noktası altında, bağlı cihazlara dayalı uygun çıktı üretir. 

Fstab Linux ve diğer Unix gibi işletim sistemlerinde, sistemdeki büyük dosya sistemleri hakkında bilgileri içeren bir yapılandırma dosyasıdır. /etc klasörünün içerisinde yer alır.

.. figure:: /img/kurulumrehberi/15.png

Aşağıdaki komutla yapılandırma dosyamızı güncelleyelim ve kontrol edelim.

>>> # genfstab -U -p /mnt >> /mnt/etc/fstab

Ardından fstab dosyamızı aşağıdaki cat komutuyla yazdırarak kontrolümüzü gerçekleştirelim.

>>> # cat /mnt/etc/fstab

>>> UUID=B8E0-3DA3                            /boot/efi      vfat    defaults,noatime 0 2

>>> UUID=b6b6baaa-baf6-4f81-bd7a-3650575bf79b /              ext4    defaults,noatime,discard 0 1

>>> UUID=7cc0e6e0-d88d-4f63-b7b3-c8edf97bc9d0 swap           swap    defaults,noatime,discard 0 2

>>> tmpfs                                     /tmp           tmpfs   defaults,noatime,mode=1777 0 0

Kök dizin Değiştirme İşlemi
***************************
Chroot, geçerli bir çalışan işlem için üzerinde olduğumuz kök dizini değiştiren bir işlemdir. Dizini değiştirdiğimiz andan itibaren bu dizin ağacının dışındaki dosyalara ve komutlara erişemezsiniz. Bu değiştirilmiş ortama ise chroot-jail denir.

Aşağıdaki komut ile chroot oluyoruz.
.. figure:: /img/kurulumrehberi/16.png

>>> # arch-chroot /mnt

Ana Bilgisayar Adını Belirleme
******************************
Bir hostname ismi bir ağdaki bir makineyi tanımlamak için oluşturulan benzersiz bir isimdir.

>>> # echo HOSTNAME > /etc/hostname

.. figure:: /img/kurulumrehberi/17.png

Yerel Ayarlar
*************
Yerel ayarlar core kütüphanesinde bulunan glibc(GNU C Library) ve yerel ayara duyarlı olan programlar tarafından yazının, zamanın, tarihin... doğru görüntülenebilmesi için kullanılır.

>>> # nano /etc/locale.gen

CTRL + W tuşlarına basarak #tr yazıp tamam tuşuna basınız

.. figure:: /img/kurulumrehberi/18.png

Yukarıdaki 2 satırın önündeki diyez(#) işaretini kaldırarak CTRL+X ile kaydedip çıkınız.

Daha sonra aşağıdaki komut ile oluşturmamızı tamamlayalım.

>>> # locale-gen

.. figure:: /img/kurulumrehberi/19.png

Sistem Yerel Ayarları
**********************
Sistem yerel ayarları çin LANG değişkenini /etc/locale.conf dosyasına yazmalıyız. Burada /etc/locale.gen dosyasında aktif etmiş olduğumuz 2 satırın ilk sütununu alacağız.

Ilk önce düzenlememiz gereken dosyayı nano editörüyle açıyoruz.

.. figure:: /img/kurulumrehberi/20.png

>>> # nano /etc/locale.conf

Aşağıdaki şekilde girdilerimizi giriyoruz ve kaydederek çıkıyoruz.

>>> LANG=tr_TR.UTF-8

Ayrıca kurulumunuza türkçe olarak devam etmek istiyorsanız aşağıdaki komut ile devam edebilirsiniz.

>>> # export LANG=tr_TR.UTF-8

Bölge Ayarları
**************
>>> # ln -sf /usr/share/zoneinfo/Europe/Istanbul /etc/localtime
.. figure:: /img/kurulumrehberi/21.png

Soft Link ve Hard Link hakkında daha fazla detay için aşağıdaki linke bakınız.

https://www.nixtutor.com/freebsd/understanding-symbolic-links/ 

Paket Yöneticisinin Yapılandırılması
************************************
Pacman libalpm(3) (Arch Linux Package Management Library)

pacman.conf pacman paket yöneticisi konfigürasyon dosyasıdır.

>>> # nano /etc/pacman.conf

.. figure:: /img/kurulumrehberi/22.png

İlk önce Multilib deposunu açıyoruz.

MultiLib 64 bit yüklemelerde, 32 bit uygulamaları çalıştırmak ve kurmak için kullanılabilecek 32 bit yazılım ve kütüphanelerini içerir.

Multilib deposunu açmak için önlerindeki diyez işaretini kaldırmamız yeterlidir.

CTRL+X diyerek kaydedip çıkıyoruz.

Ardından en hızlı yansıları bulup yansıları hızlıdan yavaşa düzenledikten sonra depoları güncellemek için aşağıdaki komutu girelim.

>>> # pacman-mirrors --fasttrack && sudo pacman -Syyu

Kullanıcı ve Şifre İşlemleri
****************************
Root parolasını değiştirmeliyiz. Aşağıdaki komut ile parolamızı değiştiriyoruz.

.. figure:: /img/kurulumrehberi/23.png

>>> # passwd

Ardından yeni bir kullanıcı ekleyelim.

.. figure:: /img/kurulumrehberi/24.png

>>> # useradd -mg users -G wheel,storage,power -s /bin/bash KULLANICI_ADI

Yeni oluşturduğumuz kullanıcının şifresini belirleyelim.

>>> # passwd KULLANICI_ADI

useradd komutuyla ilgili daha fazla bilgi için aşağıdaki linke tıklayınız.

https://www.computerhope.com/unix/useradd.htm 

Çekirdeğin Derlenmesi
*********************
>>> # mkinitcpio -p linux

Kullanıcı İzinlerinin Ayarlanması
*********************************
>>> /etc/sudoers dosyasında değişiklikler yapmamız gerekiyor.

Bu değişiklikler için aşağıdaki komut ile yapılandırma dosyamızı açalım.

>>> # nano /etc/sudoers

.. figure:: /img/kurulumrehberi/25.png

>>> KULLANICI_ADI ALL=(ALL) ALL

ekledikten sonra wheel önündeki diyez işaretini kaldıralım.

>>> %wheel ALL=(ALL) ALL

Ctrl+x ile kaydedip çıkıyoruz.

İnternet bağlantısı için Paket İhtiyaçları
******************************************
Kurulumdan sonra wifi-menu ile internete bağlanabilmemiz için aşağıdaki 2 paketi kurmamız gerekiyor.

>>> # pacman -S dialog wpa_supplicant

.. figure:: /img/kurulumrehberi/26.png

Ardından wifi driver’ımı aşağıdaki komutla bulalım.

>>> # lspci -k

>>> 03:00.0 Network controller: Intel Corporation Centrino Wireless-N 2230 (rev c4)

>>> 	Subsystem: Intel Corporation Centrino Wireless-N 2230 BGN
	
>>> 	Kernel driver in use: iwlwifi
	
>>> 	Kernel modules: iwlwifi
	
Aşağıdaki linke tıklayalım

http://linux-wless.passys.nl/query_part.php?brandname=Intel

Buradan firmware ‘in ipw2200 olduğunu bulduktan sonra AUR’da bu paketi buldum.

https://www.archlinux.org/packages/core/any/ipw2200-fw/

core deposunda olduğu için ve bu depo öntanımlı olduğundan dolayı aşağıdaki komutla direkt olarak kurabiliriz.

>>> # pacman -S ipw2200-fw

Bu konuyla ilgili aşağıdaki linklerden yararlanabilirsiniz

https://wireless.wiki.kernel.org/en/users/drivers

https://wikidevi.com/wiki/List_of_Wi-Fi_Device_IDs_in_Linux 

https://help.ubuntu.com/community/WifiDocs/WirelessCardsSupported

https://wiki.archlinux.org/index.php/Wireless_network_configuration

.. figure:: /img/kurulumrehberi/27.png

Ethernet kartı ile internete bağlı olanların DHCPD ile otomatik ip almasını aktik etmeleri için aşağıda ki adımları takip etmeleri gereklidir.

>>> # ip link

>>> 1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1000

>>>    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00

>>> 2: enp2s0: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN mode DEFAULT group default qlen 1000

>>>    link/ether 20:89:84:27:25:13 brd ff:ff:ff:ff:ff:ff

>>> 3: wlp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DORMANT group default qlen 1000

>>>    link/ether 60:36:dd:c9:e2:44 brd ff:ff:ff:ff:ff:ff

dhcpd ile otomatik ip alması için servisi aktik etmek gerekiyor.

Bunun için şu komutu girelim.

>>> # systemctl enable dhcpcd@enp2s0.service

GRUB Kurulumu Ve EFI İçin Paket İhtiyaçları
*******************************************
>>> # pacman -S grub efibootmgr dosfstools os-prober mtools

.. figure:: /img/kurulumrehberi/28.png

X kısmını kendinize göre düzenleyiniz.

>>> # grub-install –target=x86_64-efi –-bootloader-id=X –-recheck

.. figure:: /img/kurulumrehberi/29.png

>>> # grub-mkconfig -o /boot/grub/grub.cfg

.. figure:: /img/kurulumrehberi/30.png

>>> # exit

>>> # umount -a

>>> # telinit 6

.. figure:: /img/kurulumrehberi/31.png

Arch Linux Kullanabileceğiniz Ortamlar
**************************************
Sistemimizi kurduktan sonra önemli olan kısımlardan biride kullanacağımız masaüstü ortamını yada pencere yöneticimizi seçmek olacaktır. Eğer Linux dünyasına yeni atmış biriyseniz masaüstü ortamlarıyla yola başlamanızı öneriyorum. Tecrübeli bir linux kullanıcısı iseniz pencere yöneticilerini de önerebilirim.

Arch Linux Masaüstü Ortamları
    1. Cinnamon
    2. Deepin
    3. GNOME
    4. KDE Plasma
    5. LXDE
    6. MATE
    7. Xfce

Arch Linux Pencere Yöneticileri
    1. i3wm
    2. Awesome WM
    3. Xmonad
    4. Openbox
    5. Fluxbox
    6. JWM

Arch Linux Masaüstü Ortamlarının Kurulumu
*****************************************

Masaüstü ortamı kullanmak isteyen kullanıcılarımız aşağıdaki masaüstü seçeneklerinden birini seçerek kullanabilir. Daha fazla masaüstü ortamı için aşağıdaki bağlantıyı inceleyebilirsiniz.

https://wiki.archlinux.org/index.php desktop_environment#List_of_desktop_environments 

Cinnamon Kurulumu

>>> # pacman -S cinnamon

Deepin Kurulumu

>>> # pacman -S deepin deepin-extra

GNOME Kurulumu

>>> # pacman -S gnome gnome-extra

KDE Plasma Kurulumu

>>> # pacman -S plasma

LXDE Kurulumu

>>> # pacman -S lxde

MATE Kurulumu

>>> # pacman -S mate mate-extra

Xfce Kurulumu

>>> # pacman -S xfce xfce4-goodies

Arch Linux Pencere Yöneticilerinin Kurulumu

Pencere keyfini yaşamak isteyen kullanıcılarımız aşağıdaki seçeneklerden birini seçerek kurulumunu yapabilir. Daha fazla pencere yöneticisi hakkında bilgi edinmek için aşağıdaki bağlantıyı inceleyeniz.

https://wiki.archlinux.org/index.php/Window_manager  

İ3wm Kurulumu

>>> # pacman -S i3 dmenu

Awesome WM Kurulumu

>>> # pacman -S awesome

Xmonad Kurulumu

>>> # pacman -S xmonad

Openbox Kurulumu

>>> # pacman -S openbox

Fluxbox Kurulumu

>>> # pacman -S fluxbox

Xorg Kurulumları

>>> # pacman -S xorg xorg-xinit

Alsa Ses Sürücüsünün Kurulumu
*****************************
>>> # pacman -S alsa-lib alsa-utils

Görüntüleme Yöneticisinin Kurulumu
**********************************
Aşağıdaki link aracılığıyla diğer görüntüleme yöneticilerinide inceleyebilirsiniz.

https://wiki.archlinux.org/index.php/display_manager

Örnek olması amacıyla lxdm kurulumu yapalım.

>>> # pacman -S lxdm

>>> # systemctl enable lxdm.service

Türkçe Klavyenin Ayarlanması
****************************
>>> $ sudo nano /etc/X11/xorg.conf.d/00-keyboard.conf

>>> Section "InputClass"

>>>         Identifier "system-keyboard"

>>>         MatchIsKeyboard "on"

>>>         Option "XkbLayout" "tr"

>>>         Option "XkbModel" "pc105"

>>> EndSection





Ardından Secure Boot seçeneği devredışı bırakılmış bir şekilde Boot işlemini gerçekleştirin.