Uygulamaların Yüklenmesi
########################
Uçbirim Kurulumları
*******************
Urxvt Uçbirim Kurulumu
======================
>>> $ pacman -S rxvt-unicode rxvt-unicode-terminfo

Uçbirim ile ilgili yaptığımız değişiklikleri ~/.Xdefaults dosyasına kaydedeceğiz. Yapmış olduğumuz değişiklikleri görüntülemek için ise $mod+shift+r tuşlarına bastıktan sonra $mod+enter tuşları ile uçbirimi açarak bakabilirsiniz.

Editör Kurulumları
******************
Gedit Kurulumu
==============
>>> $ sudo pacman -S gedit

Diğer editörleri aşağıdaki linkten inceleyebilirsiniz.

https://wiki.archlinux.org/index.php/Category:Text_editors

Vim Kurulumu
============
>>> $ sudo pacman -S vim

Dosya Yöneticisinin Kurulumu
****************************
Ranger Kurulumu
===============
>>> $ sudo pacman -S ranger

Dosya yöneticisinde resimlerin önizlemesini görmek için şu paketi kurmalısınız.

https://aur.archlinux.org/rxvt-unicode-pixbuf.git

>>> $ ranger –-copy-config=all

>>> $ sudo gedit ~/.config/ranger/rc.conf

>>> set preview_images true

>>> set preview_images_method urxvt

Pdf Görüntüleyici Kurulumları
*****************************
Zathura Kurulumu
================
>>> $ sudo pacman -S zathura zathura-pdf-poppler

Xournal Kurulumu
================
>>> $ cd /tmp

>>> $ git clone https://aur.archlinux.org/xournal-git.git

>>> $ cd /xournal-git

>>> $ makepkg -g >> PKGBUILD

.. figure:: /img/uygulamalarinyuklenmesi/1.png

Tarayıcı Kurulumları
********************
Chromium Kurulumu
=================
>>> $ sudo pacman -S chromium

Chrome için Güzel Bir Tema
==========================
https://chrome.google.com/webstore/detail/simple-material-theme/mdnphgdednjnpcoeamekbogoblkdajep 

.. figure:: /img/uygulamalarinyuklenmesi/2.png

QuteBrowser Kurulumu
====================
>>> $ sudo pacman -S qutebrowser

>>> $ sudo pacman -S mlocate

>>> $ sudo pacman -S streamlink

>>> $ sudo pacman -S mpv

>>> $ sudo updatedb

>>> $ locate qutebrowser

Qutebrowser ayarlarını yapmak için tarayıcımızı açalım.

>>> :set

>>> /fonts

“Dejavu Sans Mono” olarak ayarlıyorum.

Yeni bir sekme açmak için t tuşuna bastığımda bunun gerçekleşmesini istiyorum.

>>> :bind t :open -t

Yukarıda bind kullanarak t tuşuna yeni bir sekme açması için atama yaptık.

Farklı aramalar yapmak için

>>> :set

>>> /searchengines

>>> {"DEFAULT": "https://duckduckgo.com/?q={}","aw":"https://wiki.archlinux.org/?search={}","g":"https://www.google.com/search?hl=en&q={}","gw":"https://wiki.gentoo.org/?search={}"}

Yukarıdaki kısmı ihtiyaçlarınıza göre düzenleyebilirsiniz. Mesela :aw Installation yazdığımda arch wiki üzerinde Installation araması yapacaktır.

;m yazdıktan sonra seçeceğiniz harflerle istediğiniz videouya en iyi kalitede izleyebileceksiniz. “ ;m “ile mpv kullanmanız için aşağıdaki ayarı yapınız.

>>> :set

bindings.commands kısmına aşağıdaki kodu ekleyiniz.

>>> {"normal": {";m": "hint links spawn mpv {hint-url}", "M": "hint links spawn mpv {hint-url}", "m": "spawn mpv {url}"}}

Artık videolarınızı aşağıdaki gibi açabilirsiniz :)

.. figure:: /img/uygulamalarinyuklenmesi/3.png

Qute browserda sekmeleri sol tarafa almak için aşağıdaki ayarlamaları yapabilirsiniz.

>>> $ sudo nano ~/.config/qutebrowser/config.py

>>> c.tabs.position = "left"

.. figure:: /img/uygulamalarinyuklenmesi/4.png

Yeni sekme için Mouse’un tekerleğine tıklayabilirsiniz.

Geçerli olan sekmeyi ise d tuşuna basarak kapatabilirsiniz.

Geçerli olan sekmenin linkini kopyalamak için yp tuşlarına basabilirsiniz.

* Ayrıca mpv kullanımı için şu hata ile karşılaşabilirsiniz.

Command exited with status 2, see :messages for details

Arch Linux Hata Çözümleri bölümüne bakınız.

DuckDuckGo BANG için aşağıdaki linkten faydalanabilirsiniz.

https://duckduckgo.com/bang

Masaüstü Duvarkağıdı Değiştirilmesi
***********************************
Feh Kurulumu
============
>>> $ sudo pacman -S feh

>>> $ sudo ~/.config/i3/config

>>> # Feh Configuration

>>> exec feh --bg-scale ~/Downloads/arch.jpg

Sohbet Araçlarının Kurulumları
******************************
Pidgin Kurulumu
===============
https://aur.archlinux.org/packages/pidgin-light/

Kurulumunu yaptıktan sonra facebook gibi sohbet araçlarını kullanmak için aşağıdaki paketi kurunuz.

>>> $ sudo pacman -S purple-plugin-pack

Irssi Kurulumu
==============
>>> $ sudo pacman -S irssi

Irssi ile Arch Linux Dünyasına Adım
===================================
irssi kullanmak için Uçbirimde aşağıdaki komutu yazınız.

>>> $ irssi

#freenode bağlantısı için şu komutu yazalım.

>>> /connect freenode

Arch Linux kanalına girmek için

>>> /join #archlinux

Ardından şu komutu giriniz.

>>> ?invite

>>> /join #archlinux

Kullanıcı adınız ve şifrenizle giriş yapmak için aşağıdaki iki komutu sırasıyla giriniz.

>>> /nick KULLANICI_ADINIZ

>>> /msg nickserv identify SIFRENIZ

Artık burada online olarak Arch linux ile ilgili sorularınızı sorabilir anlık cevaplar alarak problemlerinizi çözebilirsiniz.

>>> /help

komutuyla yardım alabilirsiniz.

>>> /quit

komutuyla çıkış yapabilirsiniz.

.. figure:: /img/uygulamalarinyuklenmesi/5.png

AUR Yardımcılarının Kurulumları
*******************************
Pikaur Kurulumu
===============
>>> $ sudo pacman -S pikaur

Uzaktan Erişim Programlarının Kurulması
***************************************
Anydesk Kurulumu
================
>>> $ pikaur -S anydesk

İndirme Yöneticilerinin Kurulumları
***********************************
Uget Kurulumu
=============
>>> $ sudo pacman -S uget

kurulumu yaptıktan sonra uget-gtk açıyorum.

Dosya --> Yeni indirme yolunu izledikten sonra indirme yapacağımız adresi URL kısmına ekleyip dizin olarakta hangi dizine kaydedilmesini istiyorsanız o dizini giriniz. Tamam butonuna basınız.

.. figure:: /img/uygulamalarinyuklenmesi/6.png

İndirmemiz başladı.

.. figure:: /img/uygulamalarinyuklenmesi/7.png

İndirmemiz bittiğinde şöyle bir ekran ile karşılaşacağız.

.. figure:: /img/uygulamalarinyuklenmesi/8.png

Office Uygulamalarının Kurulumları
********************************
Libreoffice Kurulumu
==================
>>> $ sudo pacman -S libreoffice-fresh

WPS Office Kurulumu
=================
WPS Ofis dosyasının paketlenmiş güncel sürümünü buradan indirebilirsiniz. İndirdikten sonra paketin üzerine sağ tıklayarak "Yazılım Kur" ile aç seçeneği ile kurunuz.

Kurulumdan sonra başlat menüsünde, ofis kategorisinde WPS Ofisi bulabilirsiniz. Sistem diliniz Türkçe ise varsayılan olarak Türkçe açılacaktır. Ayrıca sistem dili Türkçe ise varsayılan olarak Türkçe imla denetimi de devrede olacaktır. Bu şekilde hazır gelmesi için düzenlendi ve paketlendi.

Uyarı: Sistem diliniz Türkçe değil ise Türkçe dil seçeneği ve imla denetimi devreye girmeyecektir. Bu durumda:

Türkçe imla denetimini devreye sokmak için:

WPS Ofis uygulamasını açın ve "Gözden Geçir -> Yazım Denetimi -> Dili Ayarla" adımlarını takip edip açılan pencereden Türkçeyi seçerek etkinleştirin.

Türkçe dil tercihini etkinleştirmek için aşağıdaki görselde görüldüğü gibi sağ üst köşedeki A ikonunu tıklayın ve açılan pencereden Türkçe dilini seçip uygulamayı yeniden başlatın.

.. figure:: /img/uygulamalarinyuklenmesi/9.png

.. figure:: /img/uygulamalarinyuklenmesi/10.png

Türkçe dil dosyası ile hazırlanmamış bir WPS ofis kurduysanız aşağıdaki paketi yükleyerek Türkçe dil dosyasını ve imla denetimini tek seferde kurabilirsiniz. (tüm arch tabanlı dağıtımlar için)

>>> wpsoffice-i18n-tr-20180601-1-any.pkg.tar.xz

Archman deposunda aynı dil dosyası mevcuttur, PAMAC üzerinden veya

>>> $ pac wpsoffice-i18n-tr

komutu ile yükleyebilirsiniz.

Microsoft Office Online
=====================
Paketi pamac ile ya da uçbirimden 

>>> $ pac ms-office-online

komutu ile kurduktan sonra menüden

.. figure:: /img/uygulamalarinyuklenmesi/11.png

Microsoft Office Online uygulamasını çalıştırın ve aşağıdaki gibi oturum açıp kullanın.

.. figure:: /img/uygulamalarinyuklenmesi/12.png

.. figure:: /img/uygulamalarinyuklenmesi/13.png

.. figure:: /img/uygulamalarinyuklenmesi/14.png

Ofis Uygulamaları ve Açıklamaları Menüde Nasıl Türkçe Yapılır?
=============================================================
Öncesi:

.. figure:: /img/uygulamalarinyuklenmesi/15.png

Sonrası:

.. figure:: /img/uygulamalarinyuklenmesi/16.png

Uçbirimi açıyoruz ve aşağıdaki komutu giriyoruz.

>>> $ git clone https://github.com/Archman-OS/ofistr.git && cd ofistr && mkdir /home/$USER/.local/share/applications/ && mv * .desktop /home/$USER/.local/share/applications/

Şimdi menüyü kontrol edelim ve farkı görelim.

Manuel olarak ne yaptığımızı anlamak isterseniz durum şöyledir.

Tek komut ile https://github.com/Archman-OS/ofistr dizinindeki dosyaları indirdik ve 

KULLANICI/.local/share/applications

dizinine taşımış olduk.

Bölümleme Araçlarının Kurulması
*******************************
Gparted Kurulumu
================
>>> $ sudo pacman -S gparted

FTP Programlarının Kurulumları
******************************
Filezilla Kurulumu
==================
>>> $ sudo pacman -S filezilla

Versiyon Kontrol Sisteminin Kurulumu
************************************
Git Kurulumu
============
>>> $ sudo pacman -S git

Ekran Görüntüsü Alma Paketinin Kurulumu
***************************************
Gnome Screenshot Kurulumu
=========================
>>> $ sudo pacman -S gnome-screenshot

Ardından bunu bir kısayol tuşuna atayalım.

>>> $ sudo gedit ~/.config/i3/config

>>> bindsym $mod+p exec gnome-screenshot

şeklinde ayarlamamızı yapalım. Bundan sonra $mod+p ye bastığımızda direk olarak screenshot alabileceğiz.

Resim Görüntüleyicisi Kurulumları
*********************************
Gpicview Kurulumu
=================
>>> $ sudo pacman -S gpicview

VPN Kurulumları
***************
Openvpn ve Tor Kurulumu
=======================
>>> $ sudo pacman -S openvpn

>>>$ sudo pacman -S unzip

>>> $ cd ~/Downloads/openvpn

>>> $ sudo openvpn --config vpnbook-us1-tcp443.ovpn

root şifrenizi girin.

user name: vpnbook 

password: 5bhea6u

Yeni uçbirim açınız.

>>> $ pikaur -S tor-browser-tr

Karşılaşılan hata karşısında “p” tuşuna basınız.

Ardından VPN açık olduğu Uçbirimde “CTRL+C” basınız.

Uçbirimde

>>> $ tor-browser-tr &

* Şifre sürekli olarak güncellenmektedir.

Masaüstü Yapılandırması
***********************
Compton Kurulumu
================
>>> $ sudo pacman -S compton

>>> $ sudo ~/.config/i3/config

>>> exec_always compton -f

Kaydediyoruz.

Zip Araçlarının Kurulumları
***************************
P7zip Kurulumu
==============
>>> $ sudo pacman -S p7zip

Torrent Aracının Kurulumları
****************************
Transmission Kurulumu
=====================
>>> $ sudo pacman -S transmission-cli

>>> $ transmission-daemon

>>> $ transmission-remote -l

>>> ID     Done       Have  ETA           Up    Down  Ratio  Status       Name

>>> Sum:              None               0.0     0.0

>>> $ transmission-remote -a 'magnet:?xt=urn:btih:c5de2604156e9f6f7bc5f625fef5d7da63cc21ee&dn=archlinux-2018.11.01-x86_64.iso&tr=udp://tracker.archlinux.org:6969&tr=http://tracker.archlinux.org:6969/announce'

>>> localhost:9091/transmission/rpc/ responded: "success"

>>> $ transmission-remote -l

>>> ID     Done       Have  ETA           Up    Down  Ratio  Status       Name

>>>    1    n/a       None  Unknown      0.0     0.0   None  Downloading  archlinux-2018.11.01-x86_64.iso

>>> Sum:              None               0.0     0.0

Şimdi uçbirim yerine indirmemizi tarayıcı üzerinde görüntüleyelim.

http://localhost:9091/transmission/web/ 

.. figure:: /img/uygulamalarinyuklenmesi/17.png

Bu adrestende indirmenizin son durumunu görüntüleyebilirsiniz.
Son olarak uçbirim üzerinden son duruma bakalım.
>>> $ transmission-remote -l
>>> ID     Done       Have  ETA           Up    Down  Ratio  Status       Name
   1    88%   546.6 MB  1 min        0.0   753.0    0.0  Downloading  archlinux-2018.11.01-x86_64.iso
Sum:          546.6 MB               0.0   753.0

Web Geliştirme Araçlarının Kurulumu
***********************************
Aptana Studio Kurulumu
======================
Bugün Arch Linux'ta Aptana Studio 3 kurulumunda karşılaştığımız hatayı nasıl çözebileceğimizi ve aptana studio'yu nasıl kurabileceğimizden bahsedeceğim.

>>> $ git clone https://aur.archlinux.org/aptana-studio.git

>>> $ cd aptana-studio

>>> $ makepkg -g >> PKGBUILD

>>> $ makepkg -sri

Karşılaşılan Hata:

>>> mv: 'Aptana_Studio_3' durumlanamadı: Böyle bir dosya ya da dizin yok

>>> “==> HATA: package() içinde bir hata oluştu.” problemi

>>>    Çıkılıyor...

Hatanın çözümü için aşağıdaki komutla PKGBUILD dosyasını düzenleyin.

>>> $ sudo gedit PKGBUILD

>>> build() {

>>>   cd $srcdir

>>>   mkdir -p Aptana_Studio_3

>>>   bsdtar -xf * .zip -C Aptana_Studio_3

>>>   mkdir -p $srcdir/usr/bin/

>>>   echo 'exec /usr/lib/aptana/AptanaStudio3' > $srcdir/usr/bin/aptana-studio

>>> }

Kaydedip çıkınız ve kurulumu şu şekilde tamamlayınız.

>>> $ makepkg -sri

Aptana-studio'yu dmenu üzerinden açmaya çalıştığımda program çalışmadı. Bunun sebebini öğrenmek için uçbirim üzerinde aşağıdaki komutla programı açmaya çalıştım ve şu çıktılarla karşılaştım.

>>> $ aptana-studio

>>> /usr/bin/aptana-studio: satır 1: /usr/lib/aptana/AptanaStudio3: Erişim engellendi

>>> /usr/bin/aptana-studio: satır 1: exec: /usr/lib/aptana/AptanaStudio3: çalıştırılamıyor: Erişim engellendi

Aptana Studio Erişim Problemi

Bunun çözümü için şu komutla çalıştırma yetkisi verdim.

>>> $ sudo chmod +x /usr/lib/AptanaStudio3

“An error has occured. See the log file.” Problemi

Ardından program açıldı.Ancak yine bir hatayla karşılaştık.

.. figure:: /img/uygulamalarinyuklenmesi/18.png

Bu hatanın çözümü için Java Sürümünde bir değişiklik yapmamız gerekiyor.

jdk8 kurulumu için aşağıdaki adımları takip ediniz.

>>> $ cd /tmp

>>> $ git clone https://aur.archlinux.org/jdk8.git

>>> $ cd jdk8/

>>> $ makepkg -g >> PKGBUILD

>>> $ makepkg -sri

Bu şekilde kurulumumuzu tamamladık. İlk önce hangi java sürümümüzün aktif olduğuna bakalım.

>>> $ sudo archlinux-java status

>>> Available Java environments:

>>>   java-11-openjdk (default)

>>>  java-8-jdk

Şimdi Java-8-jdk paketimizi default hale getirelim.

>>> $ sudo archlinux-java set java-8-jdk

Ardından tekrar durumumuzu bir kontrol edelim.

>>> $ sudo archlinux-java status

>>> Available Java environments:

>>>   java-11-openjdk

>>>   java-8-jdk (default)

Güncellemelerimizi yapalım.

>>> $ sudo pacman -Syyu

olarak bilgisayarımızı yeniden başlatalım

>>> $ sudo reboot -h now

Uyarı: Java kullanan programları root olarak çalıştırırsanız javayıda root olarak çalıştırmanız gerekir. Komutlarla javayı root olarak kullanmak sisteminize zarar verebilir.

>>> $ aptana-studio

.. figure:: /img/uygulamalarinyuklenmesi/19.png

Sanallaştırma Programlarının Kurulması
**************************************
Vmware Workstation Kurulumu
===========================
Vmware Workstation Kurulumundan önce aşağıdaki bağımlılıkları doğru bir şekilde kurmamız gerekiyor.

    1. fuse2 - for vmware-vmblock-fuse
    2. gtkmm - for the GUI
    3. linux-headers - for module compilation
    4. ncurses5-compat-libs AUR - needed by the --console installer
    5. libcanberra - for event sounds
    6. pcsclite

ncurses5-compat-libs haricindeki kurulumları yapalım.

>>> $ sudo pacman -S fuse2 gtkmm linux-headers libcanberra pcsclite

Ardından ncurses5-compat-libs kurulumuna geçelim.

>>> $ cd /tmp

>>> $ git clone https://aur.archlinux.org/ncurses5-compat-libs.git 

>>> $ cd ncurses-compat-libs

>>> $ makepkg -g >> PKGBUILD

>>> $ makepkg -sri

>>>     ncurses-6.1.tar.gz ... BAŞARISIZ (bilinmeyen kamu anahtarı 702353E0F7E48EDB)

>>> ==> HATA: Bir veya daha fazla PGP imzası doğrulanamıyor!

“Bilinmeyen kamu anahtarı. Hata: Bir veya daha PGP imzası doğrulanamıyor” problemi

Bu hatanın giderilmesi için şu adımları izleyiniz.

>>> $ gpg --recv-keys 702353E0F7E48EDB

>>> $ gpg --list-keys 

>>> $ rm -rd ~/.gnupg

>>> $ gpg --recv-keys 702353E0F7E48ED8

>>> $ makepkg -sri

Bu şekilde paketlerimizi kurduk.

Şimdi Linux için Wmware Workstation indirelim.

https://www.vmware.com/products/workstation-pro/workstation-pro-evaluation.html 

İndirmiş olduğumuz dizine gidelim.

.sh uzantılı kurulum dosyasını indirdik.

>>> $ cd

>>> sudo sh Vmware-Workstation-Full-15.0.1-10737736.x86_64.bundle

Sözleşmeyi kabul edip Next butonuna tıklayınız.

.. figure:: /img/uygulamalarinyuklenmesi/20.png

Güncelleştirmeler için “Yes” diyelim ve Next butonuna tıklayalım.

.. figure:: /img/uygulamalarinyuklenmesi/21.png

Ön tanımlı olan /etc/init.d yazıp Next diyelim.

.. figure:: /img/uygulamalarinyuklenmesi/22.png

Next diyelim.

.. figure:: /img/uygulamalarinyuklenmesi/23.png

Kullanıcı adını girip Next diyelim.

.. figure:: /img/uygulamalarinyuklenmesi/24.png

Paylaşılan Sanal Makineler için dizin girelim.

.. figure:: /img/uygulamalarinyuklenmesi/25.png

Portumuzu girelim ve Next diyelim.

.. figure:: /img/uygulamalarinyuklenmesi/26.png

Lisans numaranızı giriniz.

.. figure:: /img/uygulamalarinyuklenmesi/27.png

Artık Kurulum Hazır.

.. figure:: /img/uygulamalarinyuklenmesi/28.png

Install diyerek kurulumuza başlayalım.

.. figure:: /img/uygulamalarinyuklenmesi/29.png

Kurulum Tamamlandı.

.. figure:: /img/uygulamalarinyuklenmesi/30.png

Close diyerek çıkalım.

Eğer lisans numarasını girmediyseniz programı ilk açtığınızda şu ekran ile karşılaşırsınız.

.. figure:: /img/uygulamalarinyuklenmesi/31.png

Yukarıda lisans anahtarını girmeniz için root olarak programı açmalısınız.

>>> $ sudo vmware

lisans numasını girin.

.. figure:: /img/uygulamalarinyuklenmesi/32.png

Elektronik İmza UYAP Ve UYAP Doküman Editörü
********************************************
Ulusal Yargı Ağı Projesine giriş (UYAP) için elektronik imza kullanımı zorunlu olmasa da (Mobil İmza Kullanılabilir) Uyap Doküman Editörünü kullanarak doküman imzalamak ve imzalı dokümanları UYAP a aktarmak için elektronik imza kullanmak hayatımızın bir parçası olmuştur. Arch Linux üzerinde Uyap Doküman Editörü ile mobil imzanızı tanıtıp rahatlıkla kullanabilirsiniz.

Öncelikle Elektronik imzanızı kullanabilmek için USB cihazımızın gerekli driver larını kurmalıyız.

.. figure:: /img/uygulamalarinyuklenmesi/33.png

Gerekli paketlerin bir kısmı depolarda olmasına rağmen asıl gerekli olan Akış kartların sürücüsüne ait paket AUR deposunda mevcuttur.

Öncelikle bize gerekli olan paketlerin kurulumu ile başlayalım. sisteminizde pikaur un kurulu olduğunu varsayarak aur dan ve pacman dan gerekli paketleri kuralım.

Kurulum için uçbirimde aşağıda ki komutları verelim.

>>> $ sudo pacman -S pcsclite pcsc-tools

>>> $ sudo yaourt -S akia

bu paketlerin kurulumu tamamlandıktan sonra Elektronik İmza kartımızın Linux da tanıtılmasını sağlayacak olan servisi (PCSCD yi) çalıştırmak ve sistem açılışında bu servisi otomatik aktif hale getirmemiz  gerekli. Bunun için yine uçbirimde aşağıda ki komutları verelim;

>>> $ sudo systemctl enable pcscd

>>> $ sudo systemctl start pcscd

Elektronik imzanıza ait USB stick in Linux tarafından sorunsuzca bulunup bulunmadığını kontrol etmek için yine uçbirimde aşağıda ki komutu verebilirsiniz.

>>> $ pcsc_scan

Yine Kurulum tamamlandıktan sonra uçbirimde akia komutunu vererek akış kart programını çalıştırabilirsiniz. (Java kurulumunu henüz yapmadığınız için hata alabilirsiniz)

Elektronik imza için gerekli paketlerimiz kuruldu ve hazır. Şimdi ilerde Uyap girişinde sorun yaşamamak için Oracle java 8 ‘i yükleyelim, (Aslında Open Java işimizi görüyor fakat Uyap da doküman ön izlemelerinde openjava bazen sorun çıkarıyor)



Oracle Java 8 Aur deposunda mevcut, kurulum için uçbirimde aşağıda ki komutu verelim;

>>> $ sudo pikaur -S jre

Herşey hazır Uyap Sitemine bağlanma için hazır.

Uyap Editör Java ile yazılmış olması sayesinde ufak dokunuşlarla Linux üzerinde çalışır hale gelmiştir. Bu konuda desteği için  Ahmet Kırali ya teşekkür ederim. Ubuntu için paketlenmiş editör uygulamasını güncelleyerek Arch için yeniden paketledim.

Uyap Döküman Editörünün Arch için paketlenmiş halini indirmek isterseniz aşağıdaki linki kullanabilirsiniz.

`DROPBOX İndirme Linki <https://www.dropbox.com/s/f11vosygolsvava/uyap-editor-4.0.17-1-any.pkg.tar.xz?dl=0>`_

indirme tamamlandıktan sonra uçbirimden ( indirilen dosyanın bulunduğu dizin içerisinden) aşağıda ki komutu vererek kurulumu yapalım.

>>> $ sudo pacman -U uyap-editor-4.0.17-1-a…g.tar.gz

Uyap Editör kullanım için hazır. Uyap Editöründen istediğiniz dokümanı rahatlıkla imzalayabilirsiniz.

.. figure:: /img/uygulamalarinyuklenmesi/34.png







