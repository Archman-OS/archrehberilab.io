Pencere Yöneticisi Yapılandırmaları
###################################
I3wm Yapılandırma Ayarları
**************************
i3 için .xinitrc Yapılandırması
===============================
>>> # nano ~/.xinitrc

açılan dosyayı şu şekilde kaydedelim.

>>> #!/bin/bash

>>> exec i3

Reboot edip kullanıcı adı ve şifremizi girdikten sonra aşağıdaki komutu girerek i3 pencere yöneticisini başlatabiliriz.

>>> # startx

Ardından $mod+shift+e tuşuna basarak i3’ü kapatıyoruz.

Geçici olarak Uçbirim Görünümünü Değiştirme
===========================================
>>> $ sudo gedit ~/.Xdefaults

>>> ! urxvt

>>> URxvt*geometry:                115x40

>>> !URxvt*font: xft:Liberation Mono:pixelsize=14:antialias=false:hinting=true

>>> URxvt*font: xft:Inconsolata:pixelsize=17:antialias=true:hinting=true

>>> URxvt*boldFont: xft:Inconsolata:bold:pixelsize=17:antialias=false:hinting=true

>>> !URxvt*boldFont: xft:Liberation Mono:bold:pixelsize=14:antialias=false:hinting=true

>>> URxvt*depth:                24

>>> URxvt*borderless: 1

>>> URxvt*scrollBar:            false

>>> URxvt*saveLines:  2000

>>> URxvt.transparent:      true

>>> URxvt*.shading: 10



>>> ! Meta modifier for keybindings

>>> !URxvt.modifier: super



>>> !! perl extensions

>>> URxvt.perl-ext:             default,url-select,clipboard



>>> ! url-select (part of urxvt-perls package)

>>> URxvt.keysym.M-u:           perl:url-select:select_next

>>> URxvt.url-select.autocopy:  true

>>> URxvt.url-select.button:    2

>>> URxvt.url-select.launcher:  chromium

>>> URxvt.url-select.underline: true



>>> ! Nastavuje kopirovani

>>> URxvt.keysym.Shift-Control-V: perl:clipboard:paste

>>> URxvt.keysym.Shift-Control-C:   perl:clipboard:copy



>>> ! disable the stupid ctrl+shift 'feature'

>>> URxvt.iso14755: false

>>> URxvt.iso14755_52: false



>>> !urxvt color scheme:



>>> URxvt*background: #2B2B2B

>>> URxvt*foreground: #DEDEDE



>>> URxvt*colorUL: #86a2b0



>>> ! black

>>> URxvt*color0  : #2E3436

>>> URxvt*color8  : #555753

>>> ! red

>>> URxvt*color1  : #CC0000

>>> URxvt*color9  : #EF2929

>>> ! green

>>> URxvt*color2  : #4E9A06

>>> URxvt*color10 : #8AE234

>>> ! yellow

>>> URxvt*color3  : #C4A000

>>> URxvt*color11 : #FCE94F

>>> ! blue

>>> URxvt*color4  : #3465A4

>>> URxvt*color12 : #729FCF

>>> ! magenta

>>> URxvt*color5  : #75507B

>>> URxvt*color13 : #AD7FA8

>>> ! cyan

>>> URxvt*color6  : #06989A

>>> URxvt*color14 : #34E2E2

>>> ! white

>>> URxvt*color7  : #D3D7CF

>>> URxvt*color15 : #EEEEEC

Değişiklikleri görmek için $mod+shift+r ye basalım.

İ3wm Ses Yapılandırması
=======================
>>> $ sudo pacman -S alsa-firmware alsa-utils alsa-plugins pulseaudio-alsa pulseaudio

Kurulumuzu tamamladıktan sonra config ayarlarını düzenleyelim.

>>> $ sudo gedit ~/.config/i3/config

>>> exec --no-startup-id "pulseaudio –start”

Ses Kontrolünü gerçekleştirmek için aşağıdaki paketi yükleyelim.

>>> $ sudo pacman -S pavucontrol

Geçerli ses düzeyini i3status barda görüntülemek için aşağıdaki dosyamızı düzenleyelim.

>>> $ sudo gedit /etc/i3status.conf

>>> order += "volume master"

>>> ...

>>> ...

>>> ...



>>> volume master {

>>>         format = "V: %volume"

>>>         device = "default"

>>>         mixer = "Master"

>>>         mixer_idx = 0

>>> }

Şimdi ses kontrolünü klavyeden yapabilmemiz için aşağıdaki komutları yapılandırma dosyamıza ekleyelim.

>>> $ sudo gedit ~/.config/i3/config

>>> # Pulse Audio controls

>>> bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 0 +5% #increase sound volume

>>> bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 0 -5% #decrease sound volume

>>> bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 0 toggle # mute sound

i3blocks veya i3status Kullanımı
================================
>>> $ sudo pacman -S i3status

>>> $ sudo pacman -S i3blocks

i3blocks kullanımı için

>>> $ sudo gedit ~/.config/i3/config

>>> bar {

>>>         status_command i3blocks

>>> }

i3status kullanımı için

>>> bar {

>>>         status_command i3status

>>> }

i3status bar Renklendirme
=========================
>>> $ sudo gedit ~/.config/i3/config

>>> bar {

>>>         	status_command i3status

>>> 		colors {

>>>         	background #000000

>>>         	statusline #00A5FF #that neon blue

>>>         	separator #666666



>>>         	focused_workspace  #4c7899 #285577 #ffffff

>>>         	active_workspace   #333333 #5f676a #ffffff

>>>         	inactive_workspace #333333 #222222 #888888

>>>         	urgent_workspace   #2f343a #900000 #ffffff

>>>     		}

>>> }

.. figure:: /img/pencereyapilandirmasi/1.png

i3lock Kullanımı ve i3lock Arkaplanı Değişimi
=============================================

Uçbirim üzerinde i3lock diyerek bilgisayarınızı kilitleyebilirsiniz. Şifrenizi yazıp enter’a bastıktan sonra tekrar masaüstünüze ulaşacaksınız.

İ3lock arkaplanını değiştirmek için dosyamızı yapılandıralım

>>> $ sudo gedit ~/.config/i3/config

>>> bindsym $mod+shift+x exec i3lock --color "#9999ff"

color kısmını ve hangi tuşa atama yapmak istediğinizi kendinize göre düzenleyebilirsiniz.

Değişiklikleri görmek için

>>> $mod+shift+r

yaptıktan sonra 

>>> $mod+shift+x

yaparak görüntüleyebilirsiniz.

I3wm Kullanıcı Icin Kurulumlar
******************************
Polybar Kurulumu
================
.. figure:: /img/pencereyapilandirmasi/2.png

Polybar kurulumu için aşağıdaki adımları takip ediniz.

>>> $ cd /tmp

>>> $ git clone https://aur.archlinux.org/polybar.git 

>>> $ cd polybar

>>> $ makepkg -g >> PKGBUILD

>>> $ makepkg -sri

>>> $ install -Dm644 /usr/share/doc/polybar/config $HOME/.config/polybar/config

Uçbirim üzerinden polybar’ı görüntülemeniz için şu komutu girebilirsiniz.

>>> $ polybar example

>>> $ sudo gedit $HOME/.config/polybar/launch.sh

Ardından şu kodları ekliyoruz ve kaydediyoruz.

>>> #!/usr/bin/env bash

>>> # Terminate already running bar instances

>>> killall -q polybar

>>> # Wait until the processes have been shut down

>>> while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

>>> # Launch example

>>> polybar example &

>>> echo "Bars launched..."



Çalıştırılabilir olması için şu komutu yazıyoruz.

>>> $ chmod +x $HOME/.config/polybar/launch.sh

i3 kullanan arkadaşlar yapılandırma dosyanızı şu şekilde açınız.

>>> $ sudo gedit ~/.config/i3/config

Eski barınızı yorum satırlarıyla kapattıktan sonra aşağıdaki kodu veya diğer kodu ekleyebilirsiniz.

>>> exec_always --no-startup-id $HOME/.config/polybar/launch.sh

>>> bar {

>>>     i3bar_command $HOME/.config/polybar/launch.sh

>>> }

i3-gaps Kurulumu
================
i3-gaps pencereleriniz arasında belirli boşluklar oluşturarak daha düzenli hale getirir.

>>> $ sudo pacman -S i3-gaps

>>> $ sudo gedit ~/.config/i3/config

>>> # i3-gaps

>>> gaps inner 10

>>> gaps outer 25

>>> smart_gaps on