Hata Çözümleri
######################

error status_command process exited unexpectedly (exit 1)
*********************************************************

i3status hatası alıyorsanız sağ alt tarafta geçici çözüm için şu adımları izlemeniz yeterli olacaktır.

>>> $ sudo gedit ~/.config/i3/config

Öntanımlı kod

>>> bar {

>>> 		status_command i3status

>>> }

Yukarıdaki ön tanımlı olan kodu aşağıdaki kod ile değiştiriniz.

>>> bar {

>>> 		status_command LC_ALL=C i3status

>>> }

Locale Hatası
*************
Böyle bir hata aşağıdaki gibi çıktı üretir.

>>> $ locale

>>> locale: Cannot set LC_CTYPE to default locale: No such file or directory

>>> locale: Cannot set LC_MESSAGES to default locale: No such file or directory

>>> locale: Cannot set LC_ALL to default locale: No such file or directory

>>> LANG=tr_TR.UTF-8

>>> LC_CTYPE="tr_TR.UTF-8"

>>> LC_NUMERIC="tr_TR.UTF-8"

>>> LC_TIME="tr_TR.UTF-8"

>>> LC_COLLATE="tr_TR.UTF-8"

>>> LC_MONETARY="tr_TR.UTF-8"

>>> LC_MESSAGES="tr_TR.UTF-8"

>>> LC_PAPER="tr_TR.UTF-8"

>>> LC_NAME="tr_TR.UTF-8"

>>> LC_ADDRESS="tr_TR.UTF-8"

>>> LC_TELEPHONE="tr_TR.UTF-8"

>>> LC_MEASUREMENT="tr_TR.UTF-8"

>>> LC_IDENTIFICATION="tr_TR.UTF-8"

>>> LC_ALL=

Ya da bu tarz bir hata karşısında şu çıktıyla karşılaşırız.

>>> Could not set locale Please make sure all your LC_*/LANG settings are correct

Bu hatayı gidermek için .xinitrc dosyasını düzenleyip aşağıdaki kodları eklemeliyiz.

>>> $ sudo gedit ~/.xinitrc

>>> export LC_ALL=”tr_TR.UTF-8”

>>> export LC_COLLATE=C

>>> export LANG = tr_TR.UTF-8

.xinitrc yaptığımız değişikliklerin sistemde kalıcı değişikliğe sebep olacağını unutmayınız.

Please make sure that the kernel module ‘vmmon’ is loaded.
**********************************************************

.. figure:: /img/hatacozumleri/1.png

.. figure:: /img/hatacozumleri/2.png

Problemin çözümü için aşağıdaki komutu root olarak girmeniz yeterlidir.

>>> $ sudo vmware-modconfig --console –install-all

Alsamixer ile Geçici Ses Sorununu Giderme
*****************************************
>>> $ sudo alsamixer

ses seviyesini ayarlayıp çıkınız.

>>> $ sudo alsactl store

Command exited with status 2, see :messages for details
***********************************************
.. figure:: /img/hatacozumleri/3.png

>>> :messages

ile qutebrowser’a baktığımda bu çıktı ile karşılaştım.

>>> Process stdout: Playing: https://www.youtube.com/watch?v=-ufQ56XytY8 [ytdl_hook] youtube-dl failed: not found or not enough permissions Failed to recognize file format. Exiting... (Errors when loading file)

Sorun bir paket eksikliğinden kaynaklanıyor.

Çözüm için

>>> $ sudo pacman -S youtube-dl

Tor Ağıyla bağlantı kurulamadı
******************************
.. figure:: /img/hatacozumleri/4.png

.. figure:: /img/hatacozumleri/5.png

Yada şu şekilde bağlantınız takılı kalıyor olabilir.

.. figure:: /img/hatacozumleri/6.png

Bu hatanın sebebi tarihimin yanlış olmasıydı. Bugün ayın 19’u olmasına rağmen 18 görünüyordu. Bu sorunu ortadan kaldırmak için Hatalı Saat ve Tarih Değiştirme başlığına bakabilirsiniz.

.. figure:: /img/hatacozumleri/7.png

Hatalı Saat ve Tarihi Değiştirme
********************************
Eğer i3status’da tarihiniz ve saatiniz yanlış ise şu adımları izlemeniz gerekiyor.

>>> $ sudo hwclock --set --date="2018-11-18 03:50:50"

>>> $ sudo hwclock -s

>>> $mod+shift+r

Fakeroot: Komut yok
*******************
.. figure:: /img/hatacozumleri/8.jpg

Bu sorunu yaşayan arkadaşım için paketi kendi bilgisayarımda kurup paketin sağlam olup olmadığını kesinlikle kontrol ettim.

Aşağıdaki komut ile paketimizi kurabiliyoruz.

>>> $ pikaur -S pacifica-icon-theme

Ancak ya yukarıdaki hata ile karşılaşırsanız ve Arch Linux'a yeni başlamış biriyseniz o zaman ne olacak.

>>> $ sudo pikaur -S fakeroot

>>> $ pikaur-S pacifica-icon-theme

Sorunsuz bir şekilde paketimiz yüklenecektir. Komut yok hatası ile karşılaşıyorsanız muhtemel çözümü eksik olan paketlerin kurulması ile çözebilirsiniz.

Openbox netctl problemi
***********************
.. figure:: /img/hatacozumleri/9.jpg

>>> Job for netctl@wlp3s0\x2dBARBAROS_KAS.service failed because the control process exited with error code.

>>> See "systemctl status "netctl@wlp3s0\\x2dBARBAROS_KAS.service"" and "journalctl -xe" for details.

>>> [root@barbaros barbaros]# systemctl status netctl@wlp3s0\\x2dBARBAROS_KAS.service

>>> ● netctl@wlp3s0\x2dBARBAROS_KAS.service - Networking for netctl profile wlp3s0-BARBAROS_KAS

>>> Loaded: loaded (/usr/lib/systemd/system/netctl@.service; static; vendor preset: disabled)

>>> Active: failed (Result: exit-code) since Tue 2017-04-04 15:59:36 UTC; 11s ago

>>> Docs: man:netctl.profile(5)

>>> Process: 614 ExecStart=/usr/lib/network/network start %I (code=exited, status=1/FAILURE)

>>> Main PID: 614 (code=exited, status=1/FAILURE)


>>> Nis 04 15:59:18 barbaros systemd[1]: Starting Networking for netctl profile wlp3s0-BARBAROS_KAS...

>>> Nis 04 15:59:18 barbaros network[614]: Starting network profile 'wlp3s0-BARBAROS_KAS'...

>>> Nis 04 15:59:36 barbaros network[614]: WPA association/authentication failed for interface 'wlp3s0'

>>> Nis 04 15:59:36 barbaros network[614]: Failed to bring the network up for profile 'wlp3s0-BARBAROS_KAS'

>>> Nis 04 15:59:36 barbaros systemd[1]: netctl@wlp3s0\x2dBARBAROS_KAS.service: Main process exited, code=exited, status=1/FAILURE

>>> Nis 04 15:59:36 barbaros systemd[1]: Failed to start Networking for netctl profile wlp3s0-BARBAROS_KAS.

>>> Nis 04 15:59:36 barbaros systemd[1]: netctl@wlp3s0\x2dBARBAROS_KAS.service: Unit entered failed state.

>>> Nis 04 15:59:36 barbaros systemd[1]: netctl@wlp3s0\x2dBARBAROS_KAS.service: Failed with result 'exit-code'.

netctl ve network managerı yüklemiştim. İkiside aynı amaçlı programdır. Network manageri tüm bağımlılıklarıyla birlikte sildim. Bu yüzden wpa_supplicant paketi de silindi. bu yuzden arch dual boot kullandığım için arch linux ile chroot oldum ve wpa_supplicant paketi yükledim. Reboot edip arch linux openbox'ımı açtım. Uçbirimde wifi-menu yazarak kendi internetime bağlandım.

Kernel driver not installed and FATAL: Module vboxdrv not found in directory
****************************************************************************
.. figure:: /img/hatacozumleri/10.png

Yardımcı video: https://www.youtube.com/watch?v=O694C3tsZjI 

>>> $ su

>>> # pacman -S virtualbox

>>> paket bağımlılıkları çözümleniyor...

>>> :: VIRTUALBOX-HOST-MODULES için 2 sağlayıcı mevcut:

>>> :: Depo community

>>> 1) virtualbox-host-dkms 2) virtualbox-host-modules-arch

>>> Bir sayı girin (default=1): 2

>>> # dkms autoinstall

>>> # modprobe vboxdrv

>>> modprobe: FATAL: Module vboxdrv not found in directory /lib/modules/4.9.18-1-lts

.. figure:: /img/hatacozumleri/11.png

>>> # exit

>>> $ modprobe vboxdrv

>>> $ cd builds

>>> $ git clone https://aur.archlinux.org/virtualbox-bin.git

>>> $ cd virtualbox-bin

>>> $ makepkg -g >> PKGBUILD

>>> $ makepkg -sri

>>> $ modprobe vboxdrv

>>> modprobe: ERROR: could not insert 'vboxdrv': Operation not permitted

>>> $ sudo modprobe vboxdrv

.. figure:: /img/hatacozumleri/12.png

Could not find bundler
*******************************
>>> ramarcher /opt/development/metasploit-framework $ bundle install

>>> /usr/lib/ruby/2.3.0/rubygems/dependency.rb:319:in to specs : Could not find 'bundler' (>= 0.a) among 16 total gem(s) (Gem::LoadError)

>>> Checked in 'GEM PATH=/home/ramarcher/.rvm/gems/ruby-2.3.1@metasploit-framework:/home/ramarcher/.rvm/gems/ruby-2.3.1@global', execute gem env` for more information

>>> from /usr/lib/ruby/2.3.0/rubygems/dependency.rb:328:in to spec'

>>> from /usr/lib/ruby/2.3.0/rubygems/core ext/kernel gem.rb:65:in gem'

>>> from /usr/bin/bundle:22:in <main>'

>>> ramarcher /opt/development/metasploit-framework $ bundle update

>>> /usr/lib/ruby/2.3.0/rubygems/dependency.rb:319:in to specs': Could not find 'bundler' (>= 0.a) among 16 total gem(s) (Gem::LoadError)

>>> Checked in  GEM PATH=/home/ramarcher/.rvm/gems/ruby-2.3.1@metasploit-framework:/home/ramarcher/.rvm/gems/ruby-2.3.1@global', execute  gem env` for more information

>>> from /usr/lib/ruby/2.3.0/rubygems/dependency.rb:328:in  to spec'

>>> from /usr/lib/ruby/2.3.0/rubygems/core ext/kernel gem.rb:65:in  gem'

>>> from /usr/bin/bundle:22:in  <main>'


>>> ramarcher /opt/development/metasploit-framework $

>>> ramarcher /opt/development/metasploit-framework $ gem install bundler

>>> Fetching: bundler-1.12.5.gem (100%)

>>> Successfully installed bundler-1.12.5

>>> Parsing documentation for bundler-1.12.5

>>> Installing ri documentation for bundler-1.12.5

>>> Done installing documentation for bundler after 5 seconds

>>> 1 gem installed

>>> ramarcher /opt/development/metasploit-framework $ bundle install

>>> Fetching gem metadata from https://rubygems.org/

>>> Fetching version metadata from https://rubygems.org/

>>> Fetching dependency metadata from https://rubygems.org/

>>> Resolving dependencies....

>>> Installing rake 11.2.2

>>> Installing i18n 0.7.0

>>> Using json 1.8.3

>>> Installing minitest 5.9.0

>>> Installing thread safe 0.3.5

>>> Installing builder 3.2.2

>>> ......

conky-lua-archers-git compiling error
*************************************
.. figure:: /img/hatacozumleri/13.png

Paket: https://aur.archlinux.org/packages/conky-lua-archers-git

Pakette çok küçük bir hata var.

PKGBUILD'ı düzenleyelim

>>> prepare() {

>>> cd "${srcdir}/${_pkgname}"

>>> mkdir -p build/

>>> }

Bu şekilde düzenledikten sonra kaydedelim.

Permission denied failed to initialize KVM
******************************************
.. figure:: /img/hatacozumleri/14.png

>>> # modprobe kvm-intel

>>> # groupadd kvm

>>> # gpasswd -a USERNAME kvm

>>> # chown root:kvm /dev/kvm setfacl -m g::rw /dev/kvm

>>> # setfacl -m g::rw /dev/kvm

Steam Kurulum Problemi
**********************
Steam'i Uçbirimden çalıştıramadım ve aşağıdaki hata ile karşılaştım.

>>> $ steam

>>> /home/ramazan/.local/share/Steam/steam.sh: line 154: VERSION_ID: unbound variable

>>> /home/ramazan/.local/share/Steam/steam.sh: line 154: VERSION_ID: unbound variable

>>> Running Steam on arch 64-bit

>>> /home/ramazan/.local/share/Steam/steam.sh: line 154: VERSION_ID: unbound variable

>>> STEAM_RUNTIME is enabled automatically

>>> Installing breakpad exception handler for appid(steam)/version(1468023329)

>>> libGL error: unable to load driver: i965_dri.so

>>> libGL error: driver pointer missing

>>> libGL error: failed to load driver: i965

>>> libGL error: unable to load driver: i965_dri.so

>>> libGL error: driver pointer missing

>>> libGL error: failed to load driver: i965

>>> libGL error: unable to load driver: swrast_dri.so

>>> libGL error: failed to load driver: swrast

Bu hataları gidermek için aşağıdaki komutları yazıyoruz.

>>> $ find ~/.steam/root/ \( -name "libgcc_s.so*" -o -name "libstdc++.so*" -o -name "libxcb.so*" -o -name "libgpg-error.so*" \) -print -delete

>>> $ find ~/.local/share/Steam/ \( -name "libgcc_s.so*" -o -name "libstdc++.so*" -o -name "libxcb.so*" -o -name "libgpg-error.so*" \) -print -delete

>>> $ rm -f ~/.local/share/Steam/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu/libstdc++.so.6

>>> $ rm -f ~/.local/share/Steam/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu/libgcc_s.so.1

>>> $ LC_ALL=C steam

Spice Graphics are not supported
********************************
.. figure:: /img/hatacozumleri/14.png

Çözüm için;

qemu-git paketini bağımlılıklarıyla birlikte kaldırın ve aşağıdaki paketi kurun.

https://aur.archlinux.org/packages/qemu-spice/

kvm disabled by bios
********************
KVM, linux çekirdekli sistemler üzerinde, “çekirdek seviyesinde” sanallaştırma yapmamıza imkan veren bir sanallaştırma uygulamasıdır. Diğer sanallaştırma teknolojilerinin aksine hızlı, ve çekirdek seviyesinde çalışmasından dolayıda çok güvenlidir.

>>> $ egrep '^flags.*(vmx|svm)' /proc/cpuinfo

komutu ile sisteminizin kvm ' yi destekleyip desteklemediğini görebilirsiniz.

Açılışta aldığım kvm disabled by bios hatası için bios ayarlarımı düzenledim.

Intel Virtual Technology kısmını Enable olarak ayarlayıp reboot ettikten sonra sorun düzelir.

>>> flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm epb tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts

>>> flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm epb tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts

>>> flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm epb tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts

>>> flags : fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf eagerfpu pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm epb tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts

Parçalama hatası. Çekirdek döküldü
**********************************
.. figure:: /img/hatacozumleri/15.png

wget ile archlinux-network.iso dosyasını indirmeye çalışırsak bu tarz bir hata ile karşılaşabilirsiniz.

wget’in alternatifi olan uget’i kullanacağım.

uget kurulumu için Arch Linux Uygulamalarının Yüklenmesi bölümünde uget kurulumunu inceleyebilirsiniz.

Device /dev/loop/ not initialized in udev database even after waiting 100000000 microseconds.
*********************************************************************************************
Arch Linux kurulumu grup yapılandırması esnasında böyle bir uyarıyla karşılaşıyor ve kurulumunuzu tamamlayamıyor iseniz şu adımları izleyebilirsiniz.

Karşılaşılan uyarı:

Device /dev/loop/ not initialized in udev database even after waiting 100000000 microseconds.

şeklinde başlayıp uzayıp giden bir uyarı ile karşılaşırsınız.

Öncelikle kurulumda bu işlemi sonlandırmak için Ctrl+C tuşlarına basınız.

Chroot iseniz 

>>> # exit

komutu ile çıkış yapınız ve sırasıyla aşağıdaki dört komutu sırasıyla giriniz.

>>> # mkdir /mnt/hostlvm

>>> # mount --bind /run/lvm /mnt/hostlvm

>>> # arch-chroot /mnt

>>> # ln -s /hostlvm /run/lvm

Ardından girmeniz gereken gerekli komutu başarıyla çalıştırabilirsiniz. 

>>> # grub-mkconfig -o /boot/grub/grub.cfg


