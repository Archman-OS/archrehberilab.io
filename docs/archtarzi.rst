Arch Tarzı
##########
Birazdan okuyacak olduğunuz 5 prensip, Arch Tarzı yada Arch Yolu ana başlığında incelenir. Onu en iyi anlatan slogan, KISS (Keep it Simple, Stupid).

Basitlik ve Sadelik
*******************
Pek çok GNU/Linux dağıtımı kendilerini basit olarak tarif ederler, ancak basitlik kendi içerisinde çeşitli anlamlar taşır.

Arch Linux basitliği, hafif hantallıktan uzak, gereksiz eklemelerden, düzenlemelerden yada karmaşıklıktan kaçınmış, insanlara ihtiyaçları doğrultusunda düzenleme ve esneklik imkanı sunan bir yapı olarak tanımlar. Kısaca, zarif ve sade bir yaklaşım.

Hantallıktan uzak temel sistem küçük sistem demek değildir. Bundan ziyade, temel sistem, iç içe geçmiş düzensiz kısımdan ayrı tutulmuştur. Düzgün bir biçimde düzenlenmiş ve hazırlanmış, kolay ulaşılabilir ve düzenlenebilir ayar dosyalarına sahiptir. Hantal grafiksel ayar dosyaları yoktur. Bir Arch Linux sistemi en ince detayına kadar kolayca düzenlenebilir.

Öte yandan, Arch Linux herhangi bir GNU/Linux sistemin tabiatında var olan karmaşıklığa da sahiptir. Arch Linux geliştiricileri ve kullanıcıları şunu çok iyi bilirki, sistemin karışıklığını gizlemek ve son kullanıcıdan uzak tutmak, beraberinde daha karmaşık bir sistem getirebilir. Ve bundan sakınılır.

Kolay Kod Düzenleme
*******************
Arch Linux sistemi öncelikli olarak esnek bir dizayn üzerine oturtulmuştur. Gereksiz yamalardan, otomasyonlardan ve "kullanıcı dostu" terimi altında ki kalabalıktan uzak tutulmuştur.Yazılımlarda yamalar mümkün olduğunca minimumda tutulur.

Basitlik, esneklik, kolay kod düzenleme ve sadelik, Arch gelişiminde en önde olan öncelikler olmuştur.

Konseptler, dizayn ve nitelikler Arch Yolu yaklaşımını kendilerine belge olarak kabul etmişlerdir. Geliştirici takımı Arch ın gelişimi süresince bu yolu izleme konusunda kararlıdırlar.

Açıklık
*******
Açıklık, elden ele basitlik vasıtası ile geçiyor ve Arch Linux gelişiminin prensiplerinden biri haline geliyor.

Arch Linux kolay araçlar kullanır, bu araçlar kafalarda ki açıklık ve açık görüş ile seçilir ya da inşaa edilir.

Pek çok yeni GNU/Linux kullanıcısı için açıklık anlaşılması zor, fazla gelen bir kavram iken, tecrübeli Arch Linux kullanıcıları, onu büyük bir hoşgörü ve pratiksellik ile karşılar. Kullanıcı ve sistem arasındaki bütün duvarları yıkar ve sınırları kaldırır.

Arch Linux'un açık doğası aynı zamanda oldukça fazla öğrenim fıratı sunar. Bununla birlikte tecrübeli kullanıcılar diğer kapalı sistemleri kontrol edilmesi zor olarak bulurlar.

Açıklık Prensibi her geçen gün daha fazla kitleye daha geniş üyelere hitap etmeyi sağlar. Arch Linux kullanıcıları çok ve yardımsever olarak bilinir.

Kullanıcı Merkezli
******************
Genelde pek çok GNU/Linux dağıtımı kullanıcı dostu olmaya çalışır. Buna karşılık Arch Linux'un felsefesinde kullanıcı merkezli olmak vardır.

Arch Linux sistem üzerinde onlara tamamen kontrol hakkı tanıyarak, kaliteli GNU/Linux kullanıcı kitlesi ile ilişki içerisinde olmayı hedefler.

Arch Linux kullanıcıları kendi başlarına tüm sistemi düzenleyebilirler. Sistemin kendisi bu noktada onlara yardımcı olur, tabi ki mükemmel dizayn edilmiş bazı kolay araçlar dışında.

Kullanıcı merkezli bir dizayn beraberinde "kendin yap" yaklaşımını da getirir. Arch Linux kullanıcıları problemlerin çözümlerini kendi başlarına yapma ve bunları tüm kullanıcı ve geliştiricilerle paylaşma eğilimindedir. "önce yap, sonra sor" felsefesi. Bu özellikle Arch kullanıcıları depolarındaki user-contributed paketleri için çok doğrudur.

Özgür
*****
Arch Linux gelişimindeki bir diğer prensip ise özgür seçimlerdir. Kullanıcılar sadece sistem konfigürasyonlarının ne olacağı konusunda yetkili değil aynı zamanda sistemlerinin nasıl olacağına dahi karar verme özgürlüğüne sahiptir.

Sistemi kolay tutarak, Arch Linux, sistem hakkında herhangi bir seçime özgürlük ve şans tanıdı.

Yeni kurulmuş bir Arch GNU/Linux sistemi sadece temel bileşenleri kapsar ve otomatik ayar yapmaz. Kullanıcılar sistemlerini istedikleri gibi düzenleyebilirler. Tüm sistem bileşenleri, anlık erişim, paket kaldırma veya paketi alternatifiyle değiştirmeye müsaittir.

Zengin Arch GNU/Linux deposu size özgür bir tercih sunar. Ayrıca diğer dağıtımlara nazaran, paket oluşturma süreci kolay, basit ve özgürdür. Biraz tecrübe sahibi olmanız durumunda "Arch Derleme Sistemi" ("Arch Build System" - ABS) kullanarak paketlerinizi kendiniz oluşturabilirsiniz.