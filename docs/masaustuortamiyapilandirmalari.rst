Masaüstü Ortamı Yapılandırmaları
################################
XFCE Yapılandırması
*******************
Xfce Kurulumundan önce ilk önce paketleri güncelleyiniz.

>>> # pacman -Syu

Xfce kurulumu için aşağıdaki komutları giriniz.

>>> # pacman -S xfce4 xfce4-goodies

Kurulumuzu tamamladıktan sonra ilk görünümümüz aşağıdaki gibi olacaktır.

.. figure:: /img/masaustuyapilandirmasi/1.png

İnternet bağlantılarımızı kontrol etmek amacıyla ilk önce network manager ve bunun grafiksel arayüzlü kontrolü için aşağıdaki paketlerin kurulumunu yapınız.

>>> # pacman -S networkmanager network-manager-applet

Network manager ‘ ın servislerini aşağıdaki komut ile aktif ettikten sonra bilgisayarınızı yeniden başlatınız.

>>> # systemctl enable NetworkManager.service

Şimdi ilk görünümde de dikkatinizi çektiği gibi ses kontrolümüzü sağlamak amacıyla pulseaudio’nun eklentisini panele eklemeliyiz. Bunun için

.. figure:: /img/masaustuyapilandirmasi/2.png

Panel üzerinde sağa tıklayıp Panel --> Yeni Öğeler Ekle’ye tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/3.png

Pulse şeklinde arama yapıp pulseaudio eklentisini seçip ekleyiniz.

.. figure:: /img/masaustuyapilandirmasi/4.png

Artık ses seviyemizi kontrol etmek istediğimizde sağ üst tarafta böyle bir görüntü ile karşılaşacağız.

Şimdi menümüzde ufak bir değişiklik yapalım.

Normalde ilk yüklü gelen menümüzün görünümü aşağıdaki şekildedir.

.. figure:: /img/masaustuyapilandirmasi/5.png

Ön tanımlı menümüzü kaldıralım. Kaldırmak için menü üzerinde sağ tık yapıp Kaldır seçeneğine tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/6.png

Yeni öğe ekleyelim.

.. figure:: /img/masaustuyapilandirmasi/7.png

Panel üzerinde sağa tıklayıp Panel-->Yeni Öğeler Ekle tıklayınız.

Whisker Menümüzü ekleyelim.

.. figure:: /img/masaustuyapilandirmasi/8.png

Ardından menümün logosunu değiştirmek istiyorum. Bunun için önceden ayarlamış olduğumuz bir logo dosyasını /usr/share/pixmaps dizinine atınız. Ardından Whisker menümüz üzerinde sağ tıklayıp Özellikler’e tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/9.png

Simgeye tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/10.png

Kalıp Dosyalarını seçiniz.

.. figure:: /img/masaustuyapilandirmasi/11.png

Burada seçmek istediğiniz ikonu seçiniz.

.. figure:: /img/masaustuyapilandirmasi/12.png

Şimdi sahip olmuş olduğumuz görünüme bakalım.

.. figure:: /img/masaustuyapilandirmasi/13.png

Şimdi masaüstünde yer alan simgelerdeki gri arkaplanı kaldıralım.

Bunun için şu dosyayı düzenleyiniz.

>>> # sudo vim ~/.gtkrc-2.0

Aşağıdaki kodları ekleyip kaydediniz. Ardından bilgisayarı yeniden başlatınız.

>>> style "xfdesktop-icon-view" {

>>> XfdesktopIconView::label-alpha = 0

>>> XfdesktopIconView::selected-label-alpha = 170

>>> base[NORMAL] = "#cccccc"

>>> base[SELECTED] = "#cccccc"

>>> base[ACTIVE] = "#cccccc"

>>> fg[NORMAL] = "#ffffff"

>>> fg[SELECTED] = "#000000"

>>> fg[ACTIVE] = "#000000"

>>> }

>>> widget_class "*XfdesktopIconView*" style "xfdesktop-icon-view"

Sistemimizin son görünümü aşağıdaki şekildedir.

.. figure:: /img/masaustuyapilandirmasi/14.png

Şimdi masaüstü arkaplanımızı değiştirelim.

Bunun için /usr/share/backgrounds/xfce dizinine seçeceğiniz arkaplanı atınız.

Masaüstü üzerinde sağ tık yapıp Masaüstü ayarlarına giriniz.

.. figure:: /img/masaustuyapilandirmasi/15.png

/usr/share/backgrounds/xfce dizinine atılan arkaplan resimleri burada önizlemesi ile birlikte görüntülenir.

.. figure:: /img/masaustuyapilandirmasi/16.png

Masaüstümüzün son haline bakalım.

.. figure:: /img/masaustuyapilandirmasi/17.png

Alttaki panelimizi özelleştirerek daha hoş bir görünüm elde edelim.

Alttaki panel üzerinde sağ tıklayıp Panel--> Panel Tercihleri yolunu izleyiniz.

.. figure:: /img/masaustuyapilandirmasi/18.png

Panelin otomatik olarak gizlenmesini sağlayalım.

Paneli otomatik olarak gizle seçeneğinden akıllıca seçeneğini seçiniz ve Görünüm kısmına geçiniz.

.. figure:: /img/masaustuyapilandirmasi/19.png

Ayarları bu şekilde ayarladığınız takdirde alttaki paneliniz arkaplanı şeffaf ve otomatik gizlenebilir olacaktır.

.. figure:: /img/masaustuyapilandirmasi/20.png

Masaüstümüzün görünümü aşağıdaki şekilde olacaktır.

.. figure:: /img/masaustuyapilandirmasi/21.png

Terminali açtığımızda ön tanımlı olarak sol üst tarafta açılır bunun merkezi olarak açılmasını istiyorsanız akıllı konumlandırma yapmalısınız.

.. figure:: /img/masaustuyapilandirmasi/22.png

Menü --> Tüm ayarlar yolunu izleyiniz.

.. figure:: /img/masaustuyapilandirmasi/23.png

Pencere Yöneticisi İnce Ayarları’na tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/24.png

Konumlandırma seçeneğine tıklayınız. Büyüklüğü maksimum olacak şekilde ve ekranın merkezinde olarak seçip masaüstünde tekrar uçbirimini açalım.

.. figure:: /img/masaustuyapilandirmasi/25.png

Akıllı Konumlandırma ayarları yapıldıktan sonra ekranın merkezinde açılan bir uçbirim örneğini aşağıda inceleyebilirsiniz.

.. figure:: /img/masaustuyapilandirmasi/26.png

XFCE Terminal’i kullanalım. Bunun için paneldeki öntanımlı urxvt terminali sağ tık yapıp kaldıralım. Menümüze girelim ve xfce yazıp ilgili terminali panele ekleyelim.

Menüye girip Xfce Terminal üzerinde sağ tık yapıp Panele Ekle tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/27.png

Panel 2'yi seçip Ekle butonuna tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/28.png

Ardından sağ tık → Taşı diyerek istediğiniz yere yerleştirebilirsiniz.

Uçbirimi şu şekilde özelleştirelim.

    • Saydam olsun
    • Kaydırma çubuğu olmasın
    • Menü çubuğu olmasın
    
Bu Ayarlar için uçbirim’i açtıktan sonra özellikler sekmesine giriniz.

Görünüm kısmından şeffaf artalanı seçip istediğiniz biçimde şeffaflığı ayarlayınız.

Genel		→ İmlecin yanıp sönmesini aktif ediyorum.

			→ Kaydırma çubuğu devredışı

Görünüm 	→ Menü çubuğunu yeni pencerede göster

	       	→ Araç çubuğunu yeni pencerede göster 

			→ Yeni pencerelerin çerçevesini göster

Bu yukarıdaki seçenekleri devredışı bırakıyorum.

.. figure:: /img/masaustuyapilandirmasi/29.png

Şimdi paket yöneticimizi yükleyelim

https://aur.archlinux.org/pamac-aur.git 

pamac-aur paketinin kurulumu için aşağıdaki komutları giriniz.

>>> $ cd /tmp

>>> $ git clone https://aur.archlinux.org/pamac-aur.git

>>> $ cd pamac-aur

>>> $ makepkg -g >> PKGBUILD

>>> $ makepkg -sri

Bilgisayarımızı yeniden başlattıktan sonra pamac’i panel 1’de görebilirsiniz.

.. figure:: /img/masaustuyapilandirmasi/30.png

Herhangi bir güncellemede ise aşağıdaki gibi bir uyarı ile karşılaşırsınız. Bu sayede sisteminizi sürekli güncel halde tutabilirsiniz.

.. figure:: /img/masaustuyapilandirmasi/31.png

AUR yani Arch Linux Kullanıcı Deposunu Paket yöneticimize ekleyelim ki AUR’dan kurduğumuz paketlerinde güncellemelerini takip edebilelim.

Bunun için sağ üstteki paket yöneticimize tıklayınız.

Tercihler seçeneğine tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/32.png

Ardından AUR seçeneğinden AUR Desteği etkini Açık hale getirip AUR deposundaki güncellemelere bak seçeneğinide aktifleştiriyoruz.

.. figure:: /img/masaustuyapilandirmasi/33.png

Faremizin imlecinin şeklini değiştirelim.

https://www.gnome-look.org/p/1197198/  

Buradaki imleci yüklemek istiyorum.

Hali hazırda Arch Linux’un kullanıcı depolarında bulmamız mümkün olduğu için paket yöneticimizi açalım ve bibata-cursor-theme olarak arama yapalım. Paketi seçtikten sonra uygula butonuna tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/34.png

Menü --> Tüm ayarlar yolunu izleyiniz.

.. figure:: /img/masaustuyapilandirmasi/35.png

Fare ve dokunmatik yüzey'e tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/36.png

Tema kısmından yüklemiş olduğumuz temayı seçelim.

.. figure:: /img/masaustuyapilandirmasi/37.png

Şimdi kullandığımız geçerli temayı değiştirelim.

Tüm ayarlar --> Görünüm tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/38.png

Tarz’a tıklayıp xfce-flat’i seçiyorum.

.. figure:: /img/masaustuyapilandirmasi/39.png

Bu temayı seçtikten sonra pulseaudio ve menü gibi yerlerin arkaplanı değişmedi.

.. figure:: /img/masaustuyapilandirmasi/40.png

Bunun çözümü için aşağıdaki adımları izleyiniz.

>>> $ sudo nano ~/.config/gtk-3.0/gtk.css

Aşağıdaki kodları yapıştırıp kaydediniz

>>> .xfce4-panel.panel {

>>>  background-color: #2b2e37;

>>>  text-shadow: none;

>>>  -gtk-icon-shadow: none; }

>>>  .xfce4-panel.panel button.flat, .xfce4-panel.panel button.sidebar-button {

>>>   color: #F5F5F5;

>>>   border-radius: 0;

>>>   border: none; }

>>>   .xfce4-panel.panel button.flat:hover, .xfce4-panel.panel button.sidebar-button:hover >>> {

>>>   border: none;

>>>   background-color: rgba(0, 0, 0, 0.0); }

>>>    .xfce4-panel.panel button.flat:active, .xfce4-panel.panel button.sidebar-button:active, .xfce4-panel.panel button.flat:checked, .xfce4-panel.panel button.sidebar-button:checked {

>>>     color: #ffffff;

>>>     border: none;

>>>     background-color: rgba(0, 0, 0, 0.0);

>>> }

Ardından bilgisayarınızı yeniden başlatınız.

Şimdi değişikliğimizi görüntüleyelim.

.. figure:: /img/masaustuyapilandirmasi/41.png

İkonlarımızı değiştirelim

Aşağıdaki linkten tar.gz uzantılı dosyamızı indirelim. Ardından çıkartalım.

https://www.gnome-look.org/p/1218961/

>>> $ tar xf Blue-Maia.tar.xz

Kopyalama işlemimizi yapınız.

>>> $ sudo cp -r ~/Desktop/archman/Blue-Maia /usr/share/icons

Menü --> Tüm ayarlar yolunu izleyiniz

.. figure:: /img/masaustuyapilandirmasi/42.png

Görünüm sekmesinden sekmeler kısmındaki ikon setinizi seçiniz.

.. figure:: /img/masaustuyapilandirmasi/43.png

.. figure:: /img/masaustuyapilandirmasi/44.png

Pencerelerimizi değiştirelim.

Aşağıdaki pencereyi kullanmak istiyorum.

https://www.xfce-look.org/p/1165642/  

İndirmemizi yaptıktan sonra dosyaları çıkarıp ilgili dizine taşıma işlemini yapalım.

>>> $ tar xf ChrButtons.tar.xz

>>> $ cd ChrButtons

>>> $ sudo mv ChrAdapta ChrDarkButtons ChrDarkVariant ChrBlueButtons ChrGreyButtons /usr/share/themes/

Menü --> Tüm ayarlar yolunu izleyiniz

.. figure:: /img/masaustuyapilandirmasi/45.png

Pencere yöneticisine tıklayınız.

.. figure:: /img/masaustuyapilandirmasi/46.png

Yüklediğimiz pencerelerden birini seçiniz.

