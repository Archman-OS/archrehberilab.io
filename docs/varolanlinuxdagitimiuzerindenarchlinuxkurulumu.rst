Varolan Linux Dağıtımı Üzerinden Arch Linux Kurulumu
####################################################
Giriş
*****

Bu döküman, Arch Linux’u çalışan başka bir Linux dağıtımı üzerinden yüklemek için gerekli olan ve önyükleme prosedürü (bootstrapping procedure) olarak adlandırılan işlemi açıklamaktadır. Önyükleme işleminden sonra “Kurulum Rehberi” kısmında açıklandığı gibi kurulum işlemleri gerçekleştirebilirsiniz. Çalışan bir Linux üzerinden Arch Linux’u yüklemenin şu gibi avantajları vardır.

    • Arch Linux’u bir bilgisayara uzaktan yükleme (örneğin bir sanal sunucudan)
    • Varolan Linux dağıtımını LiveCD kullanmadan bir başkasıyla değiştirmek (bu başka bir konudur)
    • Yeni bir Linux dağıtımı veya Arch Linux’u bazlı bir LiveMedia oluşturmak
    • Temel Docker konteynırı gibi bir Arch Linux chroot ortamı oluşturmak
    
Önyükleme prosedürünün hedefi “arch-install-script” paketinde yer alan betikleri(script) çalıştırabilmek ve ortamı yüklemektir. Eğer üzerinde çalıştığınız Linux dağıtımı Arch Linux tabanlı bir dağıtım ise, basitçe “arch-install-script” paketini yükleyerek amacınıza ulaşabilirsiniz. Fakat farklı bir dağıtım ise, öncelikle sisteminize Arch Linux tabanlı chroot programını yüklemeniz gerekmektedir. Ben burada Ubuntu 18.04 dağıtımı üzerinden yükleme yapmayı göstereceğim. Bootstrap işleminden önce ve sonra yapacağınız birçok adım kitapta zaten yer aldığı için çoğu yerde kitabın belli bölümlerine yönlendirme yapacağım.

Yüklemeye Hazırlık ve bootstrap sürümün indirilmesi
***************************************************
Öncelikle Arch Linux’u yükleyeceğimiz disk alanını ve Swap alanını ayırmamız gerekiyor. Eğer varolan Linux dağıtımınızın GRUB ve benzeri bootloader programlarını kullanmak istiyorsanız GRUB için ayrı bir yer ayırması yapmayın. Yani bir nevi EFI için yer ayırmayın. Ben Gparted grafiksel programı ile ayırma işlemini gerçekleştirdim. Komut satırı ile yer ayırma işlemi için “Bölümleme Araçlarının Belirlenmesi” ve “Disklerin Biçimlendirilmesi” kısımlarına bakabilirsiniz.

.. figure:: /img/varolanlinuxdagitimi/1.png

Gördüğünüz gibi ben “/dev/sda13” kısmını Arch Linux’un dosya sistemini yüklemek için, “/dev/sda8” kısmını Swap alanı için kullanacağım. Bende birçok farklı dağıtım yüklü olduğundan bu kadar fazla bölüm gözüküyor. Sizde daha az gözükebilir. Bu arada önemli bir not: Eğer bilgisayarınıza yükleme esnasında cep telefonu veya taşınabilir HDD gibi cihazlar takarsanız, sistem bunu “sda” olarak görebilir ve sizin bölümlerinizi “sdb” olarak görebilir. Buna dikkat ederek işlemlerinizi yapmalısınız.

Alan ayırma işleminden sonra artık Arch Linux’un Bootstrap paketini indirebiliriz. Bunun için pek çok link mevcut olsa da ben Türkiye merkezli bir link üzerinden gideceğim. Şu linkten “archlinux-bootstrap” ile başlayan ve “tar.gz” ile biten dosyayı el ile indirebilirsiniz:

http://ftp.linux.org.tr/archlinux/iso/2018.12.01/ 

Ben bu yazıyı yazarken sürümü değişmiş olabileceğinden linkteki veya dosyadaki “2018.12.01” şeklinde yazan sürüm numarası eski olabilir. Bunu göz önünde bulundurmanızda fayda var. Şimdi ben bu indirme işlemini komut satırı üzerinden kendi Home dizinime şu şekilde yapıyorum:

.. figure:: /img/varolanlinuxdagitimi/2.png

Komutlar:

>>> # wget -c http://ftp.linux.org.tr/archlinux/iso/2018.12.01/archlinux-bootstrap-2018.12.01-x86_64.tar.gz

Gördüğünüz gibi indirme işlemi başladı. Arch Linux’un Bootstrap hali yaklaşık 140 MB olduğundan indirme işlemi çok uzun sürmez. En son şu şekilde bir mesaj ile indirme işlemi son bulunca bir sonraki adıma geçebilirsiniz:

>>> ‘archlinux-bootstrap-2018.12.01-x86_64.tar.gz’ saved [147904918/147904918]

Arch Lınux Araçlarının Varolan Dağıtıma Yüklenmesi
**************************************************
.Pacman paket yönetim sistemi, pek çok dağıtımda çalışabilen bir programdır. Doğal olarak Ubuntu’da da bir sıkıntı çıkarmaz. Arch Linux’u önyükleme prosedürü ile yüklemek için Pacman ve bazı araçlara ihtiyaç vardır. Bu araçları Ubuntu’da “arch-install-scripts” paketini yükleyerek elde edebilirsiniz. Paketi şu şekilde yükleyebilirsiniz:

.. figure:: /img/varolanlinuxdagitimi/3.png

Komutlar:

>>> # sudo apt-get install arch-install-scripts

Böylelikle Arch Linux’u kurmak için gerekli olan programları edinmiş olduk. Şimdi artık kuruluma başlayabiliriz.

Dosyaları Kopyalama VE Chroot İle Giriş Yapma
*********************************************
.İndirdiğimiz “.tar.gz” uzantılı arşivi oluşturduğumuz dosya sistemine çıkartmamız gerekli. Bunun için önce önce root kullanıcısı olarak giriş yapıp Arch Linux için oluşturduğumuz dosya sistemini dağıtımımızdaki “/mnt” dizinine bağlayalım (benim dosya sistemim “/dev/sda13” olarak oluşturuldu):

.. figure:: /img/varolanlinuxdagitimi/4.png

Komutlar:

>>> # sudo su

>>> # mount /dev/sda13 /mnt

Daha sonra arşivimizi “/mnt” dizinine çıkartalım (arşivi Home dizinine indirdiğimi hatırlatayım):

.. figure:: /img/varolanlinuxdagitimi/5.png

Komutlar:

>>> # tar xvzf archlinux-bootstrap--x86_64.tar.gz -C /mnt

Arşiv düzgünce çıkartıldıktan sonra “/mnt” dizinine girip hangi dizinlerin olduğunu kontrol edelim:

.. figure:: /img/varolanlinuxdagitimi/6.png

“root.x86_64” dizinini görüyorsak doğru yoldayız demektir. Şimdi chroot ile giriş yapmak için aslında kısa ve uzun olmak üzere iki metod olsa da ben garantili olması açısından uzun metodu göstereceğim. Bundan önce “root_.x86_64” dizini içindekileri bir üst dizine alalım ve bu dizini silelim:

.. figure:: /img/varolanlinuxdagitimi/7.png

Komutlar:

>>> # cd root.x86_64/

>>> # mv * ..

>>> # cd ..

>>> # rmdir root.x86_64/

Şimdi “/mnt” dizinindeki dosyaları kontrol edelim. Eğer aşağıdaki gibi ise bir sıkıntı yok demektir:

.. figure:: /img/varolanlinuxdagitimi/8.png

Sıradaki adımımız sistemimizdeki bazı noktaları Arch Linux’un dosya sistemine bağlamak ve “resolv.conf” konfigürasyon dosyasını ilgili yere kopyalamak olacaktır. Bunun için şu komutları sırasıyla aynen yazın:

.. figure:: /img/varolanlinuxdagitimi/9.png

Komutlar:

>>> # mount -t proc /proc proc

>>> # mount --make-rslave --rbind /sys sys

>>> # mount --make-rslave --rbind /dev dev

>>> # mount --make-rslave --rbind /run run

>>> # cp /etc/resolv.conf etc

Şimdi Pacman’in paketleri doğru bir biçimde yükleyebilmesi için sunucuları görmesi gerek. Bunun için “/mnt/etc/pacman.d/mirrorlist” dosyasındaki bazı yorum satırlarını kaldırmamız gerekiyor. Aşağıdaki komut ile bu dosyayı Nano düzenleyicisinde açın:

.. figure:: /img/varolanlinuxdagitimi/10.png

Komutlar:

>>> # nano /mnt/etc/pacman.d/mirrorlist

Burada “## Worldwide” satırı altındaki iki satırın başındaki “#” işaretini kaldırın. Yani şu şekilde görünsün:

.. figure:: /img/varolanlinuxdagitimi/11.png

Daha sonra bunu kaydetmek için “Ctrl+O” tuş kombinasyonunu kullanın ve Enter tuşuna basın. “Ctrl+X” ile Nano düzenleyicisinden çıkın. Şimdi root(/) dizinine gelin ve “/mnt” dizinine bağladığımız dosya sistemine chroot ile giriş yapın:

.. figure:: /img/varolanlinuxdagitimi/12.png

Komutlar:

>>> # cd /

>>> # chroot /mnt /bin/bash

Gördüğünüz gibi ilk kısım köşeli parantezlerin içine alındı. Şimdi birkaç ayar yapmamız gerekli. Öncelikle şu komutu yazın:

.. figure:: /img/varolanlinuxdagitimi/13.png

Komutlar:

>>> # export PS1=”(chroot $PS1)”

Komut isteminin başına “(chroot)” yazısı geldiyse işlemimiz başarılıdır demektir.

Pacman Anahtarlığına(Keyring) İlk Değer Verme
*********************************************
Yükleme yapmaya başlamadan önce Pacman anahtarlarını yüklememiz gerekir. Bunun için aşağıdaki iki komutu vermemiz yeterlidir:

.. figure:: /img/varolanlinuxdagitimi/14.png

Komutlar:

>>> # pacman-key --init

>>> # pacman-key --populate archlinux

Çeşitli çıktılardan sonra sorun çıkaran bir yazı görmediğiniz sürece işlemimiz tamamlanmış olacak (zaten ilk komutun çıktısını yukarıda verdim). Bütün süreci sağlam bir şekilde tamamlayıp tamamlamadığınızı paket repolarını güncelleyerek öğrenebilirsiniz. Bu zaten yapmanız gereken bir işlemdir:

.. figure:: /img/varolanlinuxdagitimi/15.png

Komutlar:

>>> # pacman -Syy

Son Yapılacaklar
****************
Artık bu noktadan sonra “base” ve “base-devel” gibi temel paketlerin kurulumunu yapabilir, “fstab” dosyasını oluşturabilir, saat ve tarih ayarı yapabilir ve daha birçok şeyi yapabilirsiniz. Benim tavsiyem hazır burada internete erişimde de sıkıntı çekmiyorken grafiksel kuruluma kadar birçok şeyi yapmanızdır. Ben örnek olarak “base” ve “base-devel” paketlerinin yüklenmesi ve “fstab” dosyasının oluşturulması için gereken komutları vereyim:

>>> # pacman -S base base-devel

>>> # genfstab -p / > /etc/fstab

En son ise aşağıdaki komutla root şifresini değiştirmenizi öneriyorum:

>>> # passwd

Şimdi artık dokümandaki “Kurulum Rehberi” bölümünün “Ana Bilgisayar Adını Belirleme” konusu ve daha sonraki konularını isterseniz chroot ekranından isterseniz de Arch Linux’a fizikel olarak giriş yaparak yapabilirsiniz. Chroot ekranından çıkmak için “exit” komutunu kullanınız. Çıktıktan sonra “update-grub” ile GRUB verilerini güncellemeyi unutmayınız ki GRUB sizin Arch Linux’unuzu görebilsin:

.. figure:: /img/varolanlinuxdagitimi/16.png

Komutlar:

>>> # exit

>>> # update-grub




