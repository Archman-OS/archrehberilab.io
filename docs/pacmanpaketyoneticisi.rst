Pacman Paket Yöneticisi
#######################
Pacman Nedir
************
Pacman paket yöneticisi, Arch Linux'un ayırt edici özelliklerinden biridir. Basit bir ikili paket formatını, kullanımı kolay bir derleme sistemiyle birleştirir. Pacman paket yönetiminin amacı, paketleri ister resmi depolardan isterse kullanıcının kendi inşa ettiği veya bağımsız olarak indirdiği yerel paketler olsun kolayca yüklenmesini, kaldırılmasını ve yönetilmesini sağlamaktır.

Pacman, paket listelerini ana sunucu ile senkronize ederek sistemi güncel tutar. Bu sunucu/istemci modeli aynı zamanda kullanıcının gerekli tüm bağımlılıkları içeren basit bir komutla paketleri indirmesini/yüklemesini sağlar.

Pacman, C programlama dilinde yazılmıştır ve paketleme için `tar <https://en.wikipedia.org/wiki/tar_(computing)>`_ **tar.xz** formatını kullanır.

**Not:** `Pacman <https://www.archlinux.org/packages/?name=pacman>`_ paketi `makepkg <https://wiki.archlinux.org/index.php/Makepkg>`_ ve **vercmp** gibi araçlar içerir. **pactree** ve **checkupdates** gibi diğer faydalı araçlar  `pacman-contrib <https://www.archlinux.org/packages/?name=pacman-contrib>`_  üzerinde bulunur ( `eskiden <https://git.archlinux.org/pacman.git/commit/?id=0c99eabd50752310f42ec808c8734a338122ec86>`_ pacman'in bir parçasıydı)

Pacman Kullanımı
****************
Aşağıdaki veriler, pacman'ın gerçekleştirebileceği işlemlerin sadece küçük bir örneğidir. Daha fazlasını okumak için, `pacman ( 8 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.8>`_ 'e bakın.

**İpucu:** Daha önce diğer Linux dağıtımlarını kullananlar için yararlı bir `Pacman Rosetta <https://wiki.archlinux.org/index.php/Pacman_Rosetta>`_ makalesi var.

Paket Yükleme
=============
**Not:** 

* Paketler genellikle uygulamaya ek işlevler sağlayan bağımlılıklara sahiptir ancak bazı paketler zorunlu olmayan isteğe bağlı bağımlılıklara sahiptir. Bir paketi kurarken, pacman bir paketin isteğe bağlı bağımlılıklarını listeler, ancak pacman.log raporunda bulunmazlar. Bir paketin isteğe bağlı bağımlılıklarını görüntülemek için `#Querying package databases <https://wiki.archlinux.org/index.php/pacman#Querying_package_databases>`_  komutunu kullanın.

* Yalnızca başka bir paketin (isteğe bağlı olarak) bağımlılığını gerektiren bir paket yüklerken (yani, açıkça belirtmeniz gerekmez), **--asdeps** seçeneğini kullanmanız önerilir. Ayrıntılar için `#Kurulum prosedürü bölümüne <https://wiki.archlinux.org/index.php/pacman#Installation_reason>`_  bakın.

**Uyarı:**

Arch linuxa paketleri kurarken, sistemi yükseltmeden paket listesini yenilemekten kaçının (örneğin, bir paket artık resmi depolarda bulunmadığında). Uygulamada, bağımlılık sorunlarına yol açabileceğinden, **pacman -Syu paket_adı** yerine **pacman -Sy paket_adı** komutunu çalıştırmayın. Bkz. `Sistem bakımı # Kısmi güncellemeler desteklenmiyor <https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported>`_ ve `BBS#89328 <https://bbs.archlinux.org/viewtopic.php?id=89328>`_ .

Belirli Paketleri Yükleme
-------------------------
Tek bir paket veya bağımlılıklar dahil bir paket listesi kurmak için aşağıdaki komutu verin:

>>> # pacman -S paket_adı1 paket_adı2

Regex içeren bir paket listesi kurmak için ( `bu forum başlığına bakınız <https://bbs.archlinux.org/viewtopic.php?id=7179>`_ ):

>>> # pacman -S $(pacman -Ssq package_regex)

Bazen bir paketin farklı depolarda birden fazla sürümü vardır (örneğin, ekstra ve test). Bu örnekteki sürümü ek depodan yüklemek için, depo adının önünde tanımlanması gerekir:

>>> # pacman -S extra/paket_adı

Adlarında benzerlik paylaşan birkaç paket kurmak için küme ayracı genişletme kullanabilirsiniz. Örneğin:

>>> # pacman -S plasma-{desktop,mediacenter,nm}

Bu, ihtiyaç duyulan birçok seviyeye genişletilebilir:

>>> # pacman -S plasma-{workspace{,-wallpapers},pa}

gibi.

Paket gruplarını yükleme
------------------------
Bazı paketler, hepsi aynı anda kurulabilen bir `paket grubuna <https://wiki.archlinux.org/index.php/Package_group>`_ aittir. Örneğin, şöyle bir komut verip grubun hepsini kurabilirsiniz:

>>> # pacman -S gnome

Sizden yüklemek istediğiniz **gnome** grubundan paketleri seçmenizi isteyecektir.

Bazen bir paket grubu çok miktarda paket içerecektir ve yalnızca birkaç tanesini kurmak istediğiniz veya kurmak istemediğiniz olabilir. İstemediğiniz numaralar dışındaki tüm sayıları girmek zorunda kalmak yerine, aşağıdaki sözdizimine sahip paketleri veya paket aralıklarını seçmek veya hariç tutmak daha uygun olur:

>>> Enter a selection (default=all): 1-10 15

kurulum için 1 ila 10 ve 15 arasındaki paketleri seçecektir, ya da:

>>> Enter a selection (default=all): ^5-8 ^2

kurulum için 5 ila 8 ve 2 dışındaki tüm paketleri seçecektir.

Hangi paketlerin **gnome** grubuna ait olduğunu görmek için şunu çalıştırın:

>>> # pacman -Sg gnome

Ayrıca hangi paket gruplarının uygun olduğunu görmek için https://www.archlinux.org/groups/ adresini ziyaret edin.

**Not:** Listede bir paket sisteme zaten yüklüyse, güncel olsa bile yeniden yüklenir. Bu davranış, **--needed** seçeneği ile geçersiz kılınabilir.

Paketleri kaldırma
==================
Tek bir paketi kaldırmak için bağımlılıklarının tümünü kurulu bırakın:

>>> # pacman -R paket_adı

Bir paketi ve kurulu diğer paketlerin gerektirmediği bağımlılıkları kaldırmak için:

>>> # pacman -Rs paket_adı

Bir paketi, bağımlılıklarını ve hedef pakete bağlı tüm paketleri kaldırmak için:

**Uyarı:** Bu işlem özyinelemelidir ve potansiyel olarak ihtiyaç duyulan birçok paketi kaldırabileceğinden dikkatli kullanılmalıdır.

>>> # pacman -Rsc paket_adı

Başka bir paketin gerektirdiği bir paketi, bağımlı paketi kaldırmadan kaldırmak için:

**Uyarı:** Aşağıdaki işlem bir sistemi kırabilir ve bundan kaçınılmalıdır. Bkz. `Sistem bakımı #Bazı pacman komutlarından kaçının. <https://wiki.archlinux.org/index.php/System_maintenance#Avoid_certain_pacman_commands>`_ 

>>> # pacman -Rdd paket_adı

Pacman, bazı uygulamaları kaldırırken önemli konfigürasyon dosyalarını kaydeder ve bunları şu uzantıyla adlandırır: *.pacsave.* Bu yedekleme dosyalarının oluşturulmasını önlemek için **-n** seçeneğini kullanın:

>>> # pacman -Rn paket_adı

**Not:** Pacman, uygulamanın kendisinin oluşturduğu konfigürasyonları kaldırmaz (örneğin, ev dizinindeki klasördeki "dotfiles" nokta ile başlayan dizinler veya dosyalar).

Paketleri yükseltme
===================
**Uyarı:**

* Kullanıcıların, sistemlerini düzenli olarak güncellemek ve aşağıdaki komutu görmeden kör çalıştırmamak için `Sistem bakımı # Sistem yükseltmesi <https://wiki.archlinux.org/index.php/System_maintenance#Upgrading_the_system>`_  bölümündeki yönlendirmeleri takip etmeleri gerekir.

* Arch yalnızca tam sistem yükseltmelerini destekler. Detaylar için bkz. `Sistem bakımı # Kısmi güncellemeler desteklenmiyor <https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported>`_ ve `#Kurulum paketleri bölümü <https://wiki.archlinux.org/index.php/pacman#Installing_packages>`_ .

Pacman sistemdeki tüm paketleri tek bir komutla güncelleyebilir. Sistemin ne kadar güncel olduğuna bağlı olarak bu işlem biraz zaman alabilir. Aşağıdaki komut, depo veritabanlarını senkronize eder ve yapılandırılmış depolarda bulunmayan "yerel" paketleri hariç olmak üzere, sistemin paketlerini günceller:

>>> # pacman -Syu

Paket veritabanlarını sorgulama
===============================
Pacman, yerel paket veritabanını **-Q** bayrağıyla, senkronizasyon veritabanını **-S** bayrağıyla ve dosya veritabanını **-F** bayrağıyla sorgular. Her bayrağın ilgili alt önerileri için **pacman -Q --help** , **pacman -S --help** ve **pacman -F --help** bölümüne bakınız.

Pacman veritabanındaki paketleri arayabilir, hem paket adlarını hem de açıklamalarını arayabilir:

>>> $ pacman -Ss girdi1 girdi2 ...

Bazen, -s'in yerleşik GNİ'si (Genişletilmiş Normal İfadeler) çok fazla istenmeyen sonuçlara neden olabilir, bu nedenle yalnızca paket adıyla eşleşmesi sınırlandırılmalıdır; açıklama veya başka bir alan değil:

>>> $ pacman -Ss '^vim-'

Zaten kurulu paketleri aramak için:

>>> $ pacman -Qs girdi1 girdi2 ...

Uzak dosyalarda paket dosya adlarını aramak için:

>>> $ pacman -Fs girdi1 girdi2 ...

Belirli bir paket hakkında kapsamlı bilgi görüntülemek için:

>>> $ pacman -Si paket_adı

Yerel olarak kurulan paketler için:

>>> $ pacman -Qi paket_adı

İki -i bayrağı geçmek aynı zamanda yedekleme dosyalarının listesini ve değişiklik durumlarını da gösterir:

>>> $ pacman -Qii paket_adı

Paket tarafından yüklenen dosyaların listesini almak için:

>>> $ pacman -Ql paket_adı

Uzak bir paket tarafından yüklenen dosyaların listesini almak için:

>>> $ pacman -Fl paket_adı

Bir paket tarafından yüklenen dosyaların varlığını doğrulamak için:

>>> $ pacman -Qk paket_adı

**-k** bayrağını iki kere geçmek daha kapsamlı bir kontrol yapacaktır.

Dosya sistemindeki bir dosyanın hangi pakete ait olduğunu bilmek için veritabanını sorgulamak için:

>>> $ pacman -Qo /dosya/yolu/dosya_adı

Bir dosyanın hangi uzak pakete ait olduğunu bilmek için veritabanını sorgulamak için:

>>> $ pacman -Fo /dosya/yolu/dosya_adı

Artık bağımlılık olarak gerekmeyen tüm paketleri listelemek için (sahipsizler):

>>> $ pacman -Qdt

**İpucu:** Yukarıdaki komutu, bir işlemin artık bir paket sahipsiz kalması durumunda bilgilendirilmek üzere bir işlem sonrası `kancaya <https://wiki.archlinux.org/index.php/pacman#Hooks>`_  ekleyin. Bir paket depodan atıldığında, bir paket bırakıldığında yerel bir kurulumda yetim kaldığı için bu durum bildirilmek için kullanışlıdır (açıkça kurulmadıkça). Hiçbir yetişme bulunmadığında *"komut yürütülemedi"* hatalarını önlemek için, kancanızda **Exec** için aşağıdaki komutu kullanın:

>>> # /usr/bin/bash -c "/usr/bin/pacman -Qtd || /usr/bin/echo '=> None found.'"

Açıkça kurulmuş ve bağımlılık gerektirmeyen bütün paketleri listelemek için:

>>> $ pacman -Qet

Daha fazla örnek için bkz. `Pacman / İpuçları ve püf noktaları <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks>`_ .

Pactree
-------
Not: Pactree artık `pacman <https://www.archlinux.org/packages/?name=pacman>`_ paketinin bir parçası değil. Bunun yerine `pacman-contrib <https://www.archlinux.org/packages/?name=pacman-contrib>`_ te bulunabilir.

Bir paketin bağımlılık ağacını görüntülemek için:

>>> $ pactree paket_adı

Bir paketin bağımlı ağacını görüntülemek için, -r tersini bayrakla pactree'ye geçin ya da `pkgtools <https://aur.archlinux.org/packages/pkgtools/>`_ **AUR**'dan whoneeds kullanın.

Veritabanı Yapısı
-----------------
Pacman veritabanları normalde **/var/lib/pacman/sync** konumunda bulunur. **/etc/pacman.conf** dosyasında belirtilen her depo için orada bulunan ilgili bir veritabanı dosyası olacaktır. Veritabanı dosyaları, her paket için bir dizin içeren, örneğin, `which <https://www.archlinux.org/packages/?name=which>`_ paket için tar-gzip'li arşivlerdir:

>>> % tree which-2.20-6 

>>> which-2.20-6

>>> |-- depends

>>> ` -- desc

Bağımlı dosya, bu paketin dayandığı paketleri listeler; bunun yerine, dosya boyutu ve MD5 karması gibi paketin bir açıklaması vardır.

Paket önbelleğini temizleme
===========================
Pacman indirilen paketlerini **/var/cache/pacman/pkg/** dizininde saklar ve eski veya kaldırılmış sürümleri otomatik olarak kaldırmaz. Bunun bazı avantajları vardır:

1- Arch Linux `Arşivi <https://wiki.archlinux.org/index.php/Arch_Linux_Archive>`_ başka bir yöntemle önceki sürümü geri almaya gerek kalmadan bir `paketi düşürmeyi <#####>`_  sağlar. 

2- Kaldırılan bir paket, depodan yeni bir yükleme gerektirmeden doğrudan önbellek klasöründen kolayca yeniden yüklenebilir.

Bununla birlikte, klasörün süresiz boyutta büyümesini önlemek için kaseti düzenli aralıklarla temizlemek gerekir. 

`Pacman-contrib <https://www.archlinux.org/packages/?name=pacman-contrib>`_ paketi içinde sağlanan **paccache** betiği, en son sürüm 3 dışında, yüklü ve kaldırılmış paketlerin önbelleğe alınmış sürümlerini varsayılan olarak siler:

>>> # paccache -r

Kullanılmayan paketleri haftalık olarak atmak için **paccache.timer**'i etkinleştirin ve başlatın.

**İpucu:** Her pacman işleminden sonra bunu otomatik olarak çalıştırmak için #Hooks oluşturabilirsiniz, `örneklere bakın <https://bbs.archlinux.org/viewtopic.php?pid=1694743#p1694743>`_ .

Ayrıca kaç yeni sürüme sahip olmak istediğinizi de tanımlayabilirsiniz. Yalnızca bir geçmiş sürümü kullanmak için şunları kullanın:

>>> # paccache -rk1

Paccache eylemini kaldırılan paketlerle sınırlandırmak için **u** anahtarını ekleyin. Örneğin, kaldırılan paketlerin önbelleğe alınmış sürümlerinin tümünü kaldırmak için aşağıdakileri kullanın:

>>> # paccache -ruk0

daha fazla seçenek için **paccache -h** komutunun çıktısına bakabilirsiniz.

Pacman ayrıca önbellek ve artık veritabanı dosyalarını artık **/etc/pacman.conf** yapılandırma dosyasında listelenmeyen havuzlardan temizlemeye yarayan bazı dahili seçeneklere sahiptir. Bununla birlikte pacman, geçmiş sürümlerin çoğunda kalma imkanı sunmaz ve bu nedenle paccache varsayılan seçeneklerinden daha agresiftir. 

Şu anda yüklü olmayan tüm önbelleğe alınmış paketleri ve kullanılmayan eşitleme veritabanını kaldırmak için aşağıdakileri komuları çalıştırın:

>>> # pacman -Sc

Tüm dosyaları önbellekten kaldırmak için iki kez onay verin, bu en agresif yaklaşımdır ve önbellek klasöründe hiçbir şey bırakmaz:

>>> # pacman -Scc

**Uyarı:** Disk alanını boşaltmak zorunda olmadığınız sürece, yüklü paketlerin tüm geçmiş sürümlerinin ve kaldırılmış paketlerin tümünü önbellekten silmekten kaçınmanızda fayda var. Bu, paketleri tekrar indirmeden düşürmeyi veya yeniden yüklemeyi önleyecektir.

`pkgcacheclean <https://aur.archlinux.org/packages/pkgcacheclean/>`_ ve `pacleaner <https://aur.archlinux.org/packages/pacleaner/>`_ , ile önbelleği temizlemek için iki alternatif mevcuttur ve bunlar **AUR** depolarında bulunur.

Ek Komutlar
===========
Bir paketi kurmadan indirebilirsiniz:

>>> # pacman -Sw paket_adı

Uzak bir depodan olmayan bir 'yerel' paket kurun (ör. Paket AUR'dan):

>>> # pacman -U /dosya/yolu/paket/paket_adı-sürümü.pkg.tar.xz

Yerel paketin bir kopyasını pacman’ın önbelleğinde tutmak için, aşağıdakileri kullanın:

>>> # pacman -U file:///dosya/yolu/paket/paket_adı-sürümü.pkg.tar.xz

Bir 'uzak' paket kurun (pacman'ın yapılandırma dosyalarında belirtilen bir depodan değil):

>>> # pacman -U http://www.example.com/repo/paket_adı-sürümü.pkg.tar.xz

**-S**, **-U** ve **-R** eylemlerini engellemek için **-p** kullanılabilir.

Pacman her zaman kurulacak veya kaldırılacak paketleri listeler ve harekete geçmeden önce izin ister.

Kurulum Nedeni
==============

Pacman veri tabanı, kurulu paketleri neden kurulduğuna göre iki grupta ayırır:

*** Doğrudan yüklenenler:** Genel olarak **pacman -S** veya **-U** komutu ile kurulumuş paketler;

*** Bağımlılıklar:** Hiçbir zaman (genel olarak) bir pacman kurulum komutu ile kurulmamış olmasına rağmen, açıkça kurulmuş bir başka paketin gereksinim duyduğu ve kısmen istem dışı ama mecburen kurulmuş paketler.

Bir paketi kurarken, kurulum nedenini aşağıdakilere bağlı kalmaya zorlamak mümkündür:

>>> # pacman -S --asdeps paket_adı

**İpucu:** **--asdeps** ile isteğe bağlı bağımlılıkları ve sahipsiz bağımlılık paketlerini kaldırırsanız, pacman'in artık isteğe bağlı bağımlılıkları da kaldıracağını kabul etmiş olursunuz. Bu diğer paketlerin çalışmasını da olumsuz etkileyebilir.

Bir paketi yeniden yüklerken olsa da, geçerli yükleme nedeni varsayılan olarak korunur.

Doğrudan kurulmuş paketlerin listesi **pacman -Qe** ile gösterilebilirken, bağımlılıkların tamamlayıcı listesi **pacman -Qd** ile gösterilebilir. 

Önceden kurulu bir paketin yükleme nedenini değiştirmek için aşağıdakileri çalıştırın:

>>> # pacman -D --asdeps paket_adı

Ters işlemi yapmak için **--asexplicit** kullanın.

**Not:** **pacman -Syu paket_adı** ve **--asdeps** gibi, yükseltme yaparken **--asdeps** ve **--asexplicit** seçeneklerinin kullanması önerilmez.

Belirli bir dosyayı içeren bir paketi arayın

Dosya veritabanını senkronize et:

>>> # pacman -Fy

Bir dosyayı içeren bir paket arayın, örneğin:

>>> $ pacman -Fs pacman

core/pacman 5.0.1-4

    usr/bin/pacman

    usr/share/bash-completion/completions/pacman

extra/xscreensaver 5.36-1

    usr/lib/xscreensaver/pacman

**İpucu:** Dosya veritabanını düzenli olarak senkronize etmek için bir cron işi veya sistem zamanlayıcısı ayarlayabilirsiniz.

Gelişmiş işlevsellik için, tüm dosyaları ve ilişkili paketlerini içeren ayrı bir veritabanı kullanan **pkgfile** dosyasını yükleyin.

Pacman Yapılandırması
*********************
Pacman yapılandırmaları ve ayarları **/etc/pacman.conf** dosyasında bulunur: Bu, kullanıcının programı istediği şekilde çalışacak şekilde yapılandırdığı yerdir. Yapılandırma dosyası hakkında ayrıntılı bilgi `pacman.conf ( 5 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.conf.5>`_  'de bulunabilir.

Genel seçenekler
================
Genel seçenekler **[seçenekler]** bölümündedir. `Pacman ( 8 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.8>`_ 'i okuyun veya burada yapılabilecekler hakkında bilgi için varsayılan **pacman.conf** dosyasına bakın.

Güncellemeden Önce Sürümleri Karşılaştırma
------------------------------------------
Kullanılabilir paketlerin eski ve yeni sürümlerini görmek için **/etc/pacman.conf** adresindeki **"VerbosePkgLists"** satırına uyun.

**Pacman -Syu** çıktısı şöyle olacak:

Package (6)             Old Version  New Version  Net Change  Download Size

extra/libmariadbclient  10.1.9-4     10.1.10-1      0.03 MiB       4.35 MiB

extra/libpng            1.6.19-1     1.6.20-1       0.00 MiB       0.23 MiB

extra/mariadb           10.1.9-4     10.1.10-1      0.26 MiB      13.80 MiB

Paketi yükseltmeden Atla
------------------------
**Uyarı:** `Kısmi güncellemeler desteklenmediğinden <https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported>`_ , paketleri atlarken dikkatli olun.

Sistemi `yükseltirken <https://wiki.archlinux.org/index.php/pacman#Upgrading_packages>`_ belirli bir paketin atlanmasını sağlamak için, aşağıdaki gibi belirtin:

IgnorePkg=linux

Birden fazla paket için boşlukla ayrılmış bir liste kullanın veya ek **IgnorePkg** satırını kullanın. Ayrıca, glob desenleri kullanılabilir. Paketleri bir kez atlamak istiyorsanız, komut satırındaki **--ignore** seçeneğini de kullanabilirsiniz - bu sefer virgülle ayrılmış bir liste olacak.

İhmal edilmiş paketleri **pacman -S** kullanarak yükseltmek mümkün olacak: Bu durumda pacman, paketlerin bir **IgnorePkg** ifadesine dahil edildiğini size hatırlatacaktır.

Paket Grubunun Yükseltilmesini Engelle
--------------------------------------
**Uyarı:** `Kısmi güncellemeler desteklenmediğinden <https://wiki.archlinux.org/index.php/System_maintenance#Partial_upgrades_are_unsupported>`_, paket gruplarını atlamada dikkatli olun.

Paketlerde olduğu gibi, bütün bir paket grubunu atlamak da mümkündür:

IgnoreGroup=gnome

Dosya Yükseltmeyi Atla
----------------------
Bir **NoUpgrade** yönergesi ile listelenen tüm dosyalara, paket yükleme/yükseltme işlemi sırasında hiçbir zaman dokunulmaz ve yeni dosyalar bir **.pacnew** uzantısı ile yüklenir.

NoUpgrade=dosya/yolu/dosya

**Not:** Dosya yolu, paket arşivindeki dosyaları ifade eder. Bu nedenle, yolun başına eğik çizgi / koymayın.

Dosyaları Sisteme Yüklemeyi Atla
--------------------------------
Belirli dizinlerin yüklenmesini her zaman atlamak için bunları **NoExtract** altında listeleyin. Örneğin, `sistemd <https://wiki.archlinux.org/index.php/Systemd>`_ birimlerinin kurulumunu önlemek için şunu kullanın:

NoExtract=usr/lib/systemd/system/*

Daha sonraki kurallar önceki kuralları geçersiz kılar bunu yaparak!

**İpucu:** Pacman, yerel dağıtım veya ağartma suyu tarafından yerel ayarları temizlediğiniz bir paketi güncellerken eksik yerel ayarlar hakkında uyarı mesajları verir. **Pacman.conf** içindeki **CheckSpace** seçeneğinin yorumlanması, bu tür uyarıları bastırır, ancak boşluk kontrol işlevselliğinin tüm paketler için devre dışı bırakılacağını düşünün.

Birkaç Yapılandırma Dosyasını Koruyun
-------------------------------------
Birkaç yapılandırma dosyanız varsa (ör. Ana yapılandırma ve test deposunun etkin olduğu yapılandırma ile yapılandırma) ve yapılandırmalar arasında seçenekleri paylaşmanız gerekiyorsa, yapılandırma dosyalarında belirtilen Dahil Et yani **Include** seçeneğini kullanabilirsiniz.

Include = /dosya/yolu/bileşen/ayarlar

Burada /dosya/yolu//bileşen/ayarlar dosyası her iki yapılandırmada da aynı seçenekleri içerir.

Hooks - Kancalar
----------------
Pacman, işlem öncesi ve sonrası kancaları **/usr/share/libalpm/hooks/** dizininden çalıştırabilir; **pacman.conf**'daki **HookDir** seçeneğiyle, **/etc/pacman.d/hooks** varsayılan olarak daha fazla dizin belirtilebilir. Kanca yani Hook  dosya adları **.hook** ile sonlandırılmalıdır.

Pacman kancaları, örneğin, paketlerin kurulumu sırasında sistem kullanıcılarını ve dosyaları otomatik olarak oluşturmak için **systemd-sysusers** ve **systemd-tmpfiles** ile birlikte kullanılır. Örneğin, **tomcat8** paketi, tomcat8 adlı bir sistem kullanıcısının ve bu kullanıcının sahip olduğu belirli dizinlerin olmasını istediğini belirtir. Pacman kancaları **systemd-sysusers.hook** ve **systemd-tmpfiles.hook**, **pacman tomcat8** paketinin kullanıcıları ve tmp dosyalarını belirten dosyaları içerdiğini belirlediğinde **systemd-sysusers**'leri ve **systemd-tmpfiles**'i çağırır.

Alpm kancaları hakkında daha fazla bilgi için, bkz. `Alpm kancaları ( 5 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/alpm-hooks.5>`_.

Depolar ve Yansılar
===================
Özel [seçenekler] bölümünün yanı sıra, **pacman.conf** içindeki her biri **[section]**, kullanılacak bir paket deposunu tanımlar. Depo, fiziksel olarak bir veya daha fazla sunucuda depolanan mantıklı bir paket koleksiyonudur: bu nedenle her sunucuya depo için bir yansı denir.

Depolar `resmi <https://wiki.archlinux.org/index.php/Official_repositories>`_ ve `gayri resmi https://wiki.archlinux.org/index.php/Unofficial_user_repositoriesRL>` arasında ayrım yapmaktadır. Yapılandırma dosyasındaki havuzların sırası önemlidir; İlk olarak listelenen depolar, iki depodaki paketlerin sürüm numaralarından bağımsız olarak aynı isimlere sahip olması durumunda dosyada daha sonra listelenenlere göre öncelikli olacaktır. Bir depo ekledikten sonra kullanmak için önce tüm sistemi yükseltmeniz gerekir.

Her bir depo bölümü, Yansı listesinin doğrudan veya özel bir harici dosyada **Include** direktifiyle tanımlanmasına izin verir: örneğin, resmi depo yansıları **/etc/pacman.d/mirrorlist** adresinden alınmıştır. Yansı yapılandırması için `Yansılar <https://wiki.archlinux.org/index.php/Mirrors>`_ makalesine bakın.

Paket güvenliği
---------------
Pacman, paketlere ekstra bir güvenlik katmanı ekleyen paket imzalarını destekler. Varsayılan yapılandırma olan **SigLevel = Required DatabaseOptional** , global düzeydeki tüm paketler için imza doğrulamasını mümkün kılar: bu, depo başına **SigLevel** satırları tarafından geçersiz kılınabilir. Paket imzalama ve imza doğrulaması hakkında daha fazla bilgi için `pacman-anahtarına <https://wiki.archlinux.org/index.php/Pacman/Package_signing>`_ bakın.

Sorun Giderme
*************
**İşlem gerçekleştirilemedi (çakışan dosyalar)" hatası**

Aşağıdaki hatayı görürseniz:

>>> $ error: could not prepare transaction

>>> $ error: failed to commit transaction (conflicting files)

>>> $ package: /path/to/file exists in filesystem

>>> $ Errors occurred, no packages were upgraded.

yada

>>> $ hata: işlem hazırlanamadı

>>> $ hata: işlem gerçekleştirilemedi (çakışan dosyalar)

>>> $ paket: /dosya/yolu/falanca_dosya dosya sisteminde var

>>> $ Hata oluştu, paket yükseltilmedi.

Bu oluyor çünkü pacman bir dosya çakışması tespit etti ve tasarım gereği sizin için dosyaların üzerine yazmayacak. Bu tasarım gereği, normal bir davranış biçimidir, bir hata yani kusur değildir.

Sorunun çözümü genellikle basittir. Güvenli bir yol, ilk önce dosyanın başka bir paketin olup olmadığını kontrol etmektir **(pacman -Qo /dosya/yolu/dosya)**. Dosya başka bir pakete aitse, bir `hata raporu <https://wiki.archlinux.org/index.php/Bug_reporting_guidelines>`_ verin. Dosya başka bir pakete ait değilse, 'dosya sisteminde' mevcut olan dosyayı yeniden adlandırın ve güncelleme komutunu yeniden verin. Her şey yolunda giderse, dosya kaldırılabilir.

Bir programı pacman kullanmadan manuel olarak kurmuşsanız, örneğin **make install** ile bu programı tüm dosyaları ile kaldırmanız/silmeniz gerekir. Ayrıca bkz. `Pacman ipuçları #Herhangi bir pakete ait olmayan dosyaları belirleme <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Identify_files_not_owned_by_any_package>`_ 

Yüklü her paket, bu paket hakkında meta veriler içeren bir **/var/lib/pacman/local/package-version/files** dosyası sağlar. Bu dosya bozulursa, boşalırsa veya kaybolursa, paketi güncellemeye çalışırken **dosya sistemi çakışma hatalarıyla** sonuçlanır.  Böyle bir hata genellikle sadece bir paket ile ilgilidir. Söz konusu pakete ait tüm dosyaları el ile yeniden adlandırmak ve daha sonra kaldırmak yerine, pacman'i **glob** ile eşleşen yani dosya sisteminde mevcut diye uyarı veren dosyaların üzerine yazmaya zorlamak için doğrudan buradaki örnekteki gibi

>>> $ pacman -S --overwrite glob paket

komutu ile paketi kurabilirsiniz. Eskiden olduğu gibi artık **--force** parametresi desteklenmemektedir. Bunun yerine **--overwrite** kullanmalısınız.

**Uyarı:** Genellikle **--overwrite** anahtarını kullanmaktan kaçının. Bkz. `Sistem bakımı#Bazı pacman komutlarından kaçınmak <https://wiki.archlinux.org/index.php/System_maintenance#Avoid_certain_pacman_commands>`_ 

"İşlem gerçekleştirilemedi (geçersiz veya bozuk paket)" hatası
==============================================================
**/var/cache/pacman/pkg** dosyalarına (kısmen indirilmiş paketler) bakın ve bunları kaldırın (genellikle **pacman.conf**'da özel bir **XferCommand** kullanılması nedeniyle olur).

>>> # find /var/cache/pacman/pkg/ -iname  "* .part" -exec rm {} \;

"İşlem başlatılamadı (veritabanı kilitlenemiyor)" hatası
========================================================
Pacman paket veritabanını değiştirmek üzereyken, örneğin bir paket kurmak,  **/var/lib/pacman/db.lck** adresinde bir kilit dosyası oluşturur. Bu, başka bir pacman örneğinin paket veritabanını aynı anda değiştirmeye çalışmasını önler.

Veritabanını değiştirirken pacman yarıda kesilirse, bu eski kilit dosyası kalabilir. Hiçbir pacman örneğinin çalışmadığından eminseniz, kilit dosyasını silin:

>>> # rm /var/lib/pacman/db.lck

Paketler kurulum sırasında alınamıyor
=====================================
Bu hata genellikle **sync db, Hedef bulunamadı** veya **dosya alınamadı** olarak ortaya çıkıyor.

İlk olarak, paketin gerçekten var olduğundan emin olun. Paketin mevcut olduğundan emin olmanız durumunda, paket listeniz eski olabilir. Tüm paket listelerini yenilemeyi ve yükseltmeyi zorlamak için **pacman -Syu** komutunu çalıştırmayı deneyin. Ayrıca seçilen aynaların güncel olduğundan ve depoların doğru şekilde yapılandırıldığından emin olun.

Ayrıca, paketi içeren depo sisteminizde etkinleştirilmemiş olabilir; paket **multilib** deposunda olabilir, ancak **pacman.conf** dosyanızda multilib etkin değildir.

Ayrıca bkz. `SSS # Resmi depolarda neden her paylaşılan kütüphanenin sadece tek bir versiyonu var? <https://wiki.archlinux.org/index.php/Frequently_asked_questions#Why_is_there_only_a_single_version_of_each_shared_library_in_the_official_repositories?>`_ 

Pacman'i manuel olarak yeniden yükleme
======================================
**Uyarı:** Bu yaklaşımı kullanarak sisteminizi daha da kötü hale getirmek son derece kolaydır. Yükseltme sırasında #Pacman'den gelen yöntem bir seçenek değilse, bunu yalnızca son çare olarak kullanın. Bakınız: `Pacman yükseltme sırasında çöküyor <https://wiki.archlinux.org/index.php/pacman#Pacman_crashes_during_an_upgrade>`_ 

Pacman çok bozuk olsa bile, en son paketleri indirerek ve doğru konumlara çıkararak elle düzeltebilirsiniz. Gerçekleştirilmesi gereken zor adımlar şöyle:

1- Yüklenecek **pacman** bağımlılıklarını belirleyin

2- Her bir paketi seçtiğiniz bir **yansıdan** indirin

3- Her paketi kök dizinine çıkart

4- Paket veritabanını uygun şekilde güncellemek için bu paketleri **pacman -S --overwrite** ile yeniden yükleyin.

5- Tam bir sistem yükseltmesi yapın

Elinizde sağlıklı bir Arch sistemine sahipseniz, bağımlılıkların tam listesini aşağıdakilerle görebilirsiniz:

$ pacman -Q $(pactree -u pacman)

Ancak, sorununuza bağlı olarak yalnızca birkaç tanesini güncellemeniz gerekebilir. Bir paket ayıklamak için bir örnek:

# tar -xvpwf package.tar.xz -C / --exclude .PKGINFO --exclude .INSTALL --exclude .MTREE --exclude .BUILDINFO

Etkileşimli mod için **w** işaretinin kullanımına dikkat edin. Etkileşimsel olmayan bir şekilde çalıştırılması çok risklidir çünkü önemli bir dosyanın üzerine yazabilirsiniz. Ayrıca paketleri doğru sırayla çıkartmaya da dikkat edin (ilk önce bağımlılıklar). `Bu forum yazısı <https://bbs.archlinux.org/viewtopic.php?id=95007>`_ , yalnızca birkaç pacman bağımlılığının bozulduğu bu sürecin bir örneğini içerir.

Pacman yükseltme sırasında çöküyor
==================================
Pacman'in paketleri çıkarırken "veritabanı yazma" hatasıyla çökmesi ve paketleri yeniden yüklemek veya yükseltmek başarısız olursa, aşağıdakileri yapın:

1- Arch yükleme ortamını kullanarak önyükleyin. Tercihen, pacman versiyonunun sistemden daha yeni olması için yeni bir ortam kullanın.

2- Sistemin kök dosya sistemini, ör. root olarak **mount /dev/sdaX /mnt** komutunu verin ve bağlama noktasının **df -h** ile yeterli alana sahip olduğunu kontrol edin.

3- Proc, sys ve dev dosya sistemlerini de bağlayın: **mount -t proc proc /mnt/proc; mount --rbind /sys /mnt/sys; mount --rbind /dev /mnt/dev**

4- Sistem varsayılan veritabanı ve dizin konumlarını kullanıyorsa, artık sistemin pacman veritabanını güncelleyebilir ve bunu **pacman --sysroot /mnt -Syu** aracılığıyla root olarak yükseltebilirsiniz.

5- Güncellemeden sonra, yükseltilmemiş, ancak hala kırılmış paketler var ise kontrol etmenin bir yolu: **find /mnt/usr/lib -size 0**

6- Daha sonra hala bozulmuş olan herhangi bir paketin **pacman --sysroot /mnt -S package** ile tekrar kurulması ile takip edilir.

Yeniden başlatmanın ardından "Kök sürücü-aygıt bulunamadı" hatası
=================================================================
Büyük olasılıkla, çekirdek güncellemeniz sırasında **initramfs** bozulmuştur (pacman'in **--force** seçeneğinin yanlış kullanılması bir neden olabilir). İki seçenek var; ilk önce Fallback modunda sistemi başlatıp girmeyi deneyin.

**İpucu:** Fallback girişini kaldırmanız durumunda, bootloader menüsü (Syslinux için) veya **e** (GRUB veya systemd-boot için) göründüğünde her zaman **Tab** tuşuna basabilir, **initramfs-linux-fallback.img** adını değiştirip **Enter** tuşuna basabilirsiniz. veya **b** (yeni yükleyiciye bağlı olarak) yeni parametrelerle önyüklemek için basınız.

Sistem başladıktan sonra, initramfs görüntüsünü yeniden oluşturmak için bu komutu konsoldan veya bir terminalden çalıştırın (mevcut linux çekirdeği için):

>>> # mkinitcpio -p linux

Bu işe yaramazsa, geçerli bir Arch sürümünden (CD / DVD veya USB disk), kök ve önyükleme bölümlerinizi `bağlayın <https://wiki.archlinux.org/index.php/File_systems#Mount_a_file_system>`_ . Ardından arch-chroot kullanarak `chroot <https://wiki.archlinux.org/index.php/Chroot>`_   yapın:

>>> # arch-chroot /mnt

>>> # pacman -Syu mkinitcpio systemd linux

**Not:** 

* Şu anki sürümünüz yoksa veya yalnızca bir "canlı" Linux dağıtımınız varsa, eski moda yöntemini kullanarak chroot yapabilirsiniz. Açıkçası, sadece arch-chroot betiğini çalıştırmaktan daha fazlası olacak.

* Pacman ile başarısız olursa, ana bilgisayar çözülemedi, uyarıları alırsanız lütfen `internet bağlantınızı kontrol edin <https://wiki.archlinux.org/index.php/Network_configuration#Check_the_connection>`_ .

* Arch-chroot veya chroot ortamına giremiyorsanız ancak paketleri tekrar kurmanız gerekiyorsa, root bölümünüzde pacman kullanmak için **pacman --sysroot /mnt -Syu foo bar** komutunu kullanabilirsiniz.

Çekirdeği (linux paketi) yeniden yüklemek, initramfs görüntüsünü **mkinitcpio -p linux** ile otomatik olarak yeniden oluşturur. Bunu ayrı ayrı yapmaya gerek yoktur.

Daha sonra **exit, umount /mnt/{boot,}** çalıştırmanız ve **reboot** yeniden başlatmanız önerilir. Yani chroottan çıkış, bağladıklarımızı ayırma ve yeniden başlatma.

"User <email@example.org>" imzası güvenilir değil, yükleme başarısız oldu
=========================================================================
Çözüm için şu ikisinden birini deneyebilirsiniz:

* Bilinen anahtarları, yani **pacman-key --refresh-keys** güncellemesi yapın.

* Önce `archlinux-keyring <https://www.archlinux.org/packages/?name=archlinux-keyring>`_ paketini el ile yükseltin, yani **pacman -Sy archlinux-keyring && pacman -Su**

*  `pacman-key#Resetting all the keys <https://wiki.archlinux.org/index.php/Pacman/Package_signing#Resetting_all_the_keys>`_  bağlantısını takip edin.

PGP anahtarlarını içe aktarma isteği
====================================
Eski bir ISO kalıbı ile Arch'ı yüklüyorsanız, büyük olasılıkla PGP anahtarlarını içe aktarmanız istenir. Devam etmek için anahtarı indirmeyi kabul edin. PGP anahtarını başarıyla ekleyemiyorsanız, anahtarı güncelleyin veya archlinux-anahtarlığını yükseltin (yukarıya bakın).

Hata: "0123456789ABCDEF" anahtarı uzaktan alınamadı
===================================================
Paketler, yalnızca son zamanlarda archlinux anahtarlığına eklenmiş yeni anahtarlarla imzalanmışsa, bu anahtarlar güncelleme sırasında yerel olarak mevcut değildir (tavuk-yumurta sorunu). Yüklenen archlinux-keyring, güncellenene kadar anahtarı içermez. Pacman, bunu bir anahtar sunucusu aracılığıyla yapılan arama ile atlamaya çalışır; vekillerin veya güvenlik duvarlarının ardında ve belirtilen hatayla sonuçlanır. Yukarıda gösterildiği gibi önce archlinux-keyring'i yükseltin.

"User <email@archlinux.org>" imzası geçersiz, yükleme başarısız oldu
====================================================================
Sistem saati hatalı olduğunda, imzalama anahtarlarının süresi dolmuş (veya geçersiz) kabul edilir ve paketlerdeki imza kontrolleri aşağıdaki gibi bir hata vererek başarısız olur:

>>> error: package: signature from "User <email@archlinux.org>" is invalid

>>> error: failed to commit transaction (invalid or corrupted package (PGP signature))

>>> Errors occured, no packages were upgraded.

veya

>>> hata: libxslt: signature from "Jan de Groot <info@jandegrootict.nl>" is unknown trust

>>>  /var/cache/pacman/pkg/libxslt-1.1.29+42+gac341cbd-1-i686.pkg.tar.xz dosyası bozuk (geçersiz veya bozuk paket (PGP imzası)).

>>> Silinmesini istiyor musunuz? [E/h] e

>>> hata: işlem gerçekleştirilemedi (geçersiz veya bozuk paket (PGP imzası))

>>> Hata oluştu, hiçbir paket güncellenmedi.

gibi..

`Sistem saatini <https://wiki.archlinux.org/index.php/System_time>`_ düzelttiğinizden emin olun, örneğin **ntpd -qg** root olarak çalıştırın ve sonraki kurulumlardan veya yükseltmelerden önce **hwclock -w** komutunu root olarak çalıştırın.

"Uyarı: geçerli yerel ayar geçersiz; varsayılan" C "yerel ayarını kullanma hatası"
==================================================================================
Hata mesajının dediği gibi, yerel ayarınız doğru yapılandırılmamış. `Yerel Ayarlara <https://wiki.archlinux.org/index.php/Locale>`_ bakınız.

Pacman proxy ayarlarına uymuyor
===============================
İlgili ortam değişkenlerinin ($http_proxy, $ftp_proxy vb.) ayarlandığından emin olun. Sudo ile pacman kullanıyorsanız, bu ortam değişkenlerini pacman'a iletmek için `sudo'yu yapılandırmanız gerekir. <https://wiki.archlinux.org/index.php/Sudo#Environment_variables>`_ 

Bir şeyin açıkça kurulup kurulmadığına ya da bağımlılık durumuna ilişkin bilgileri koruyarak tüm paketleri nasıl yeniden kurabilirim?
=====================================================================================================================================
Tüm yerel paketleri yeniden yüklemek için: **pacman -Qnq | pacman -S**  ( **-S** seçeneği, kurulum nedenini varsayılan olarak korur).

Daha sonra **pacman -Qmq** ile listelenebilecek tüm yabancı paketleri tekrar kurmanız gerekecek.

"Paylaşılan nesne dosyası açılamıyor" hatası
============================================
Önceki pacman işlemi kaldırılmış veya pacman'ın kendisi için gerekli olan paylaşılan kütüphaneleri bozmuş gibi görünüyor.

Bu durumdan kurtulmak için, gerekli kütüphaneleri dosya sisteminize manuel olarak açmanız gerekir. İlk önce hangi paketin cevapsız kütüphaneyi içerdiğini bulun ve sonra pacman önbelleğinde bulun **( /var/cache/pacman/pkg/ )**. Gerekli paylaşılan kütüphaneyi dosya sistemine açın. Bu pacman'ın çalışmasına izin verecek. Şimdi kırık paketi yeniden yüklemeniz gerekiyor. Sadece sistem dosyalarını açarken ve pacman'in bilmediğinden --overwrite parametresini kullanmanız gerektiğini unutmayın. Pacman, paylaşılan kütüphane dosyamızı paketten bir taneyle doğru şekilde değiştirecektir.

Bu kadar, sistemin geri kalanını güncelleyin.

Paket indirme işlemlerinin dondurulması
=======================================
Pacman'in depoları güncellemesini ve senkronize etmesini önleyen ağ sorunları ile ilgili bazı sorunlar ortaya çıkabilir. Arch Linux'u yerel olarak kurarken, varsayılan pacman dosya indiricisini bir alternatif ile değiştirerek bu sorunlar çözüldü (daha fazla ayrıntı için bkz. `Pacman performansını iyileştirme <https://wiki.archlinux.org/index.php/Pacman/Tips_and_tricks#Performance>`_ ). Arch Linux'u VirtualBox'a misafir işletim sistemi olarak kurarken, bu özellik aynı zamanda makine özelliklerinde **NAT** yerine **Host** arayüzü kullanılarak da ele alınmıştır.

'core.db' dosyası yansıdan alınamadı
====================================
Bu hata iletisini doğru yansılarla birlikte alırsanız, farklı bir `sunucu <https://wiki.archlinux.org/index.php/Domain_name_resolution>`_ ayarlamayı deneyin.

Pacman Çalışma Anlayışı
***********************
Paket yükleme / yükseltme / kaldırma sırasında ne olur?
=======================================================
pacman, bir işlemde sıraya konan tüm paketler için kurulacak paket dosyasını alır.

1- pacman, paketlerin monte edilebileceği konusunda çeşitli kontroller yapar

2- önceden var olan pacman PreTransaction kancaları geçerliyse, bunlar yürütülür

3- Her paket sırayla kurulur / yükseltilir / kaldırılır

4- Paketin bir yükleme betiği varsa, pre_install işlevi yürütülür (veya yükseltilmiş veya kaldırılmış bir paket durumunda pre_upgrade veya pre_remove)

5- pacman, tüm dosyaları paketin önceden var olan bir sürümünden siler (yükseltilmiş veya kaldırılmış bir paket durumunda). Bununla birlikte, bazı yapılandırma dosyaları farklı şekilde ele alınır.

6- pacman paketi kaldırır ve dosyalarını dosya sistemine atar (yüklü veya yükseltilmiş bir paket durumunda)

7- Paketin bir yükleme betiği varsa, post_install işlevi yürütülür (veya yükseltilmiş veya kaldırılmış bir paket durumunda post_upgrade veya post_remove)

8- işlemin sonunda bulunan pacman PostTransaction kancaları geçerli olursa, bunlar yürütülür.

Ayrıca bakınız
**************
`Pacman Anasayfa <https://www.archlinux.org/pacman/>`_ 

`libalpm( 3 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/libalpm.3>`_
    
`pacman( 8 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.8>`_
    
`pacman.conf( 5 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/pacman.conf.5>`_
    
`repo-add( 8 ) <https://jlk.fjfi.cvut.cz/arch/manpages/man/repo-add.8>`_

Bu çalışmanın orijinal kaynağı: 
===============================
https://wiki.archlinux.org/index.php/pacman
